#import <UIKit/UIKit.h>
#import "News.h"

// ячейка, которая будет отображать новость
@interface NewsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *logoView;
@property (nonatomic, weak) IBOutlet UILabel *labelTitle;
@property (nonatomic, weak) IBOutlet UILabel *labelText;

- (void)setupNews:(News *)news;

@end
