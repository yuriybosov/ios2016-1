//
//  DataManager.m
//  lesson 21_1_custom_cell
//
//  Created by Yuriy Bosov on 4/6/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "DataManager.h"
#import "News.h"

@implementation DataManager

// реализация паттерна синглтон в Objective-C
+ (DataManager *)sharedInstance
{
    static DataManager *manager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}

- (NSArray *)newsList
{
    // если массив новостей еще не создан - создать его и наполнить новостями
    if (!newsList)
    {
        // создаем массив
        newsList = [[NSMutableArray alloc] init];
        
        // добавляем в него новости, вычитываем их из плиста.
        // для этого:
        // 1. на основе plist-файла создамим массив с "сырыми данными"
        // 1.1. нахоим путь к файлу News.plist (используем для этого класс NSBundle и метод pathForResource:ofType:
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"News" ofType:@"plist"];
        // 1.2. на основе пути к файлу инициализируем массив с помощью метода arrayWithContentsOfFile:
        NSArray *rawData = [NSArray arrayWithContentsOfFile:filePath];
        // 2. На основе "сырых данных" создаем модели News
        for (NSDictionary *newsRawData in rawData)
        {
            News *news = [[News alloc] init];
            
            news.title = [newsRawData objectForKey:@"title"];
            news.text = [newsRawData objectForKey:@"text"];
            news.logo = [newsRawData objectForKey:@"logo"];
            
            [newsList addObject:news];
        }
    }
    
    return newsList;
}

@end
