#import "NewsCell.h"

@implementation NewsCell

- (void)setupNews:(News *)news
{
    self.labelTitle.text = news.title;
    self.labelText.text = news.text;
    self.logoView.image = [UIImage imageNamed:news.logo];
}

@end
