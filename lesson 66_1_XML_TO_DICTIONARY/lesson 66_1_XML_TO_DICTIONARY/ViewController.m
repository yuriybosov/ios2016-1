//
//  ViewController.m
//  lesson 66_1_XML_TO_DICTIONARY
//
//  Created by Yurii Bosov on 10/19/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

@interface ViewController () <NSXMLParserDelegate> {
    AFHTTPSessionManager *sessionManager;
    NSURLSessionDataTask *currentTask;
    
    NSMutableDictionary * currentNode;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:@"https://maps.googleapis.com"];
    sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    sessionManager.responseSerializer = [[AFXMLParserResponseSerializer alloc] init];
    
    NSString *searchText = @"нахимова 80";
    NSDictionary *params = @{@"key":@"AIzaSyA95K7QmsKvc0eQgBHllEkg6hPSiIZ26bE",
                             @"address":searchText,
                             @"language":@"ru"};
    
    currentTask = [sessionManager GET:@"maps/api/geocode/xml"
                           parameters:params
                             progress:nil
                              success:^(NSURLSessionDataTask * _Nonnull task, NSXMLParser *responseObject)
                   {
                       if ([responseObject isKindOfClass:[NSXMLParser class]]) {
                           
                           responseObject.delegate = self;
                           [responseObject parse];
                           
                       }
                   }
                              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                   {
                       ;// OS_ACTIVITY_MODE : disable
                   }];
}

#pragma mark - NSXMLParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    currentNode = [[NSMutableDictionary alloc] init];
    [currentNode setObject:@"RootNode" forKey:@"name"];
    [currentNode setObject:[[NSMutableArray alloc]init] forKey:@"childs"];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSLog(@"node = %@", currentNode);
    
//    NSMutableDictionary *ttt = [NSMutableDictionary new];
////    [currentNode enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL * _Nonnull stop) {
////        if ([key isEqualToString:@"name"]) {
////            
////        }
////    }];
//    NSMutableDictionary *d = [NSMutableDictionary new];
//    NSMutableArray *a = [NSMutableArray new];
//    
//    NSString *name = [currentNode objectForKey:@"name"];
//    if (name) {
//        [ttt setObject:d forKey:name];
//    }
//    NSDictionary *attr = [currentNode objectForKey:@"attributes"];
//    if (attr) {
//        [d setValuesForKeysWithDictionary:attr];
//    }
//    NSArray *chld = [currentNode objectForKey:@"childs"];
//    if (chld)
//        [d setObject:(nonnull id) forKey:@""];
}

///

//- (NSString *)nameForChildNode:(NSArray *)childNode {
//    return []
//}

///


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    ;
    
    NSMutableDictionary *newNode = [[NSMutableDictionary alloc] init];
    
    [newNode setObject:currentNode forKey:@"parentNode"];
    
    [newNode setObject:elementName forKey:@"name"];
    if (attributeDict.count) {
        [newNode setObject:attributeDict forKey:@"attributes"];
    }
    [newNode setObject:[NSMutableArray new] forKey:@"childs"];
    
    [[currentNode objectForKey:@"childs"] addObject:newNode];
    
    currentNode = newNode;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [currentNode setObject:[string copy] forKey:@"value"];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(nullable NSString *)namespaceURI qualifiedName:(nullable NSString *)qName {
    
    NSMutableDictionary *temp = [currentNode objectForKey:@"parentNode"];
    if ([[currentNode objectForKey:@"childs"] count] == 0) {
        [currentNode removeObjectForKey:@"childs"];
    }
    [currentNode removeObjectForKey:@"parentNode"];
    currentNode = temp;
}

@end
