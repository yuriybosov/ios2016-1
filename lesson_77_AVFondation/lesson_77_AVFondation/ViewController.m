//
//  ViewController.m
//  lesson_77_AVFondation
//
//  Created by Yurii Bosov on 11/28/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AFNetworking.h"

#define kStreamURL @"https://r4---sn-n8v7sne7.googlevideo.com/videoplayback?ip=188.123.230.137&key=yt6&dur=166.394&lmt=1480059849889214&id=o-ADMCMEwFCGehB0f4OC1FUYnDTC2jDQwAi1vF4sCeWfz_&itag=22&requiressl=yes&ipbits=0&mm=31&mn=sn-n8v7sne7&ei=blE8WJ7KBYulYI32roAF&ms=au&mt=1480347836&mv=m&signature=266F0618370BFF939F649E4B389D123C9F218F35.239581254DCBFDF90CB1F9368CDB7B6266CCA146&expire=1480369614&initcwndbps=1067500&ratebypass=yes&source=youtube&pl=23&nh=IgpwcjAxLnN2bzAzKgkxMjcuMC4wLjE&upn=b0bGh7JSvUU&mime=video%2Fmp4&sparams=dur%2Cei%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&title=%D0%AD%D1%82%D0%BE+%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE+%D1%83%D0%B1%D0%B8%D0%B2%D0%B0%D0%B5%D1%82+iPhone"

#define kDownloadURL @"http://r4---sn-5hne6nlk.googlevideo.com/videoplayback?source=youtube&mt=1480355116&mv=m&mime=video%2Fmp4&signature=66AD0B46D3364954BD920AE396008BAA17D4CCDC.91EF0025CF9A4214862C15FB249581116AAADEDA&ms=au&dur=6.222&id=o-AJfe1GEuSfzUPpfBbpBM7fRpyMtwTFXtarJSVDCDuaJk&initcwndbps=2132500&pl=24&nh=IgpwZjAxLmFtczE1Kg4xNDkuMTQuMTQyLjEwNQ&sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Csource%2Cupn%2Cexpire&itag=18&ip=149.13.117.213&mm=31&mn=sn-5hne6nlk&ratebypass=yes&expire=1480376993&key=yt6&upn=Vue6-c4QlHU&ipbits=0&lmt=1432293557663002&title=%D0%92%D1%8B%D0%BA%D1%80%D1%83%D1%82%D0%B8%D0%BB%D1%81%D1%8F+-+%286+%D1%81%D0%B5%D0%BA%29"

#define kVladStreamURL @"http://92.50.181.6/tv/1021/index.m3u8?token=om7inuyo9cllywb0dug7tckvwi27wkn0"

@interface ViewController () {
    AVPlayerViewController *playerController;
    MPMoviePlayerController *mpPlayer;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // сомнительное решение!!!
    playerController = [self.childViewControllers firstObject];
    
    [self playVideBySegmentIndex:0];
}


- (void)playVideBySegmentIndex:(NSInteger)segmentIndex {
    
    if (segmentIndex == 0) {
        // play offile
        // получаем путь на локальный файлик
        NSString *path = [[NSBundle mainBundle] pathForResource:@"video" ofType:@"mov"];
        NSURL *url = [NSURL fileURLWithPath:path];
        
        AVPlayer *player = [AVPlayer playerWithURL:url];
        playerController.player = player;
        [playerController.player play];
        
    } else if (segmentIndex == 1){
        // play online
        NSURL *url = [NSURL URLWithString:kStreamURL];
        AVPlayer *player = [AVPlayer playerWithURL:url];
        playerController.player = player;
        [playerController.player play];
        
    } else {
        // download and play
        
        // путь для сохранения файла
        NSString *fileName = @"myVideo.mp4";
        NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
        NSString *fullPath = [path stringByAppendingPathComponent:fileName];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:kDownloadURL]];
        
        NSURLSessionDownloadTask *task =  [[AFHTTPSessionManager manager] downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
            
            NSLog(@"%lld / %lld", downloadProgress.completedUnitCount, downloadProgress.totalUnitCount);
            
        } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
            return [NSURL fileURLWithPath:fullPath];
            
        } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
            
            if  (filePath) {
                AVPlayer *player = [AVPlayer playerWithURL:filePath];
                playerController.player = player;
                [playerController.player play];
            }
        }];
        
        [task resume];
    }
}

#pragma mark - Action

- (IBAction)segmentedValueChanges:(UISegmentedControl *)sender {
    [self playVideBySegmentIndex:sender.selectedSegmentIndex];
}

- (IBAction)steamButtonClicked:(id)sender {
    
//    NSURL *url = [NSURL URLWithString:kVladStreamURL];
//    AVPlayer *player = [AVPlayer playerWithURL:url];
//    playerController.player = player;
//    [playerController.player play];
    
    NSURL *stream = [NSURL URLWithString:kVladStreamURL];
    mpPlayer = [[MPMoviePlayerController alloc] initWithContentURL:stream];
    
    UIView *playerView = mpPlayer.view;
    
    [playerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addSubview:playerView];
    
    [self.view addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-0-[playerView]-0-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(playerView)]];
    
    [self.view addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-0-[playerView]-0-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(playerView)]];
    [mpPlayer play];
}

@end
