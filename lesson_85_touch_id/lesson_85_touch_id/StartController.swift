//
//  StartController.swift
//  lesson_85_touch_id
//
//  Created by Yurii Bosov on 1/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import LocalAuthentication

class StartController: UIViewController {

    @IBOutlet weak var retrayButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.retrayButton.isHidden = true
    }
    
    @IBAction func retrayButtonClicked(_ sender: UIButton) {
        self.authenticationWithBiometrics()
    }
    
    deinit { // тоже самое что и -(void)dealloc
        print("deinit StartController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDefaults.standard.bool(forKey: "use_touch_id") {
            
            self.authenticationWithBiometrics()
            
        } else {
            //
            let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
            UIApplication.shared.keyWindow?.rootViewController = tabbar
        }
    }
    
    func authenticationWithBiometrics() {
        // вход по touch id
        LAContext().evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Для входа в приложение", reply: { (success: Bool, error: Error?) in
            
            DispatchQueue.main.async {
                if success {
                        let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbar")
                        UIApplication.shared.keyWindow?.rootViewController = tabbar
                } else {
                    self.retrayButton.isHidden = false
                }
            }
        })
    }
}
