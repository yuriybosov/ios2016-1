//
//  SettingsController.swift
//  lesson_85_touch_id
//
//  Created by Yurii Bosov on 1/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import LocalAuthentication

class SettingsController: UIViewController {

    @IBOutlet weak var touchSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Settings"
        
        // disable touchSwitch if touch id not available
        if !LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil){
            touchSwitch.isEnabled = false
        } else {
            touchSwitch.isOn = UserDefaults.standard.bool(forKey: "use_touch_id")
        }
    }
    
    @IBAction func switchChangedValue(_ sender: UISwitch) {
        
        if  touchSwitch.isOn {
            // autorize with touch id
            
            let context = LAContext()
            
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Для входа в приложение", reply: { (success: Bool, error: Error?) in
                
                DispatchQueue.main.async {
                    
                    if success {    // подтвердил вход по touch id
                        // save flag on
                        UserDefaults.standard.set(true, forKey: "use_touch_id")
                        UserDefaults.standard.synchronize()
                        
                    } else {        // отклонил вход по touch id
                        self.touchSwitch.setOn(false, animated: true)
                    }
                }
            })
            
            
        } else {
            // save flag off
            UserDefaults.standard.set(false, forKey: "use_touch_id")
            UserDefaults.standard.synchronize()
        }
    }
}
