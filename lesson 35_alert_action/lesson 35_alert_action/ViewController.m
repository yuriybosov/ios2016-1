//
//  ViewController.m
//  lesson 35_alert_action
//
//  Created by Yuriy Bosov on 6/1/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)showComfirmationAlert
{
    __weak ViewController *weakSelf = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Вы уверены?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [weakSelf clerTextField];
                               }];
    
    [alert addAction:actionCancel];
    [alert addAction:actionOK];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Actions

- (void)clerTextField
{
    textField.text = nil;
    clearButton.enabled = NO;
}

- (IBAction)clearButtonPressed:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    __weak ViewController *weakSelf = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Внимание" message:@"Очистить поле ввода?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
    {
        [weakSelf showComfirmationAlert];
    }];
    
    [alert addAction:actionCancel];
    [alert addAction:actionOK];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)textFieldDidChangeValue:(UITextField *)sender
{
    clearButton.enabled = textField.text.length > 0;
}

- (IBAction)hideKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

@end
