
#import "ViewController.h"
#import "TableViewCellClick.h"

@implementation ViewController

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCellClick *cellClick = [tableView dequeueReusableCellWithIdentifier:@"cellClick"];
    
    cellClick.selectionStyle = UITableViewCellSelectionStyleNone;
    cellClick.delegate = self;
    
    return cellClick;
}

#pragma mark - UITableViewDelegate
// реализация ячеек с высотой не равной стандартной (т.е. != 44 пункта)
/*
 метод 1 (для случаю, когда все ваши ячейки имеют одинаковую высоту)
 для это переопределяем метод tableView:heightForRowAtIndexPath: и возвращаем в нем нужную нам высоту (к примеру 60 пунктов)
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

/*
 метод 2 (для случая, когда все ваши ячейки имеют разную высоту в зависимости от содержания)
 для это:
 - переопределяем метод tableView:heightForRowAtIndexPath: и
 возвращаем в нем значение UITableViewAutomaticDimension - т.е. что высота
 будет определятся "автоматически"
 - расчитываем высоту ячейки и возвращаем ее в методе tableView:estimatedHeightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    
    // расчет высоты (будет рассмотрен на след. занятии)
    
    return height;
}
*/

#pragma mark - TableViewCellClickProtocol

- (void)didClickedButtonInCell:(UITableViewCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSLog(@"indexPath %@", indexPath);
}
 
@end
