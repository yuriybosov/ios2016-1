//
//  AppDelegate.h
//  lesson 21_custom_cell
//
//  Created by Yuriy Bosov on 4/4/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

