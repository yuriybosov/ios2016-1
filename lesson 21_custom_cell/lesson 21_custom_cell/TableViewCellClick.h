
#import <UIKit/UIKit.h>

@protocol TableViewCellClickProtocol <NSObject>

- (void)didClickedButtonInCell:(UITableViewCell*)cell;

@end




@interface TableViewCellClick : UITableViewCell

@property (nonatomic, weak) id<TableViewCellClickProtocol> delegate;

@end
