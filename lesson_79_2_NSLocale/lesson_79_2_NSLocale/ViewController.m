//
//  ViewController.m
//  lesson_79_2_NSLocale
//
//  Created by Yurii Bosov on 12/5/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *lb1;
    IBOutlet UILabel *lb2;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // вывод текущего времени, использую nsdateformatter
    NSDate *date = [NSDate date]; // получение текущего, системного времени. C нулевым отклонением по GMT
    NSLog(@"date %@", date);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    dateFormatter.timeStyle = NSDateFormatterLongStyle;
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:2*60*60];
    lb1.text = [dateFormatter stringFromDate:date];
    
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en"];
    lb2.text = [dateFormatter stringFromDate:date];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
