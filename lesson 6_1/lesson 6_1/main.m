
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        //класс для работы с числами
        NSInteger a = 5;
        NSNumber *A = [NSNumber numberWithInteger:a];
        NSLog(@"A = %@", A);
        
        CGFloat b = 6.123;
        NSNumber *B = [NSNumber numberWithFloat:b];
        NSLog(@"B = %@", B);
        
        NSNumber *C = @(-1);
        NSLog(@"C = %@", C);
        
        // метод numberWith... можно заменить на @()
        
        // сравнение NSNumber
        if ([A isEqualToNumber:B])
        {
            NSLog(@"числа равны");
        }
        else
        {
            NSLog(@"числа НЕ равны");
        }
        
        // получние из NSNumber простых типов
        NSInteger aa = [A integerValue];
        CGFloat bb = [B floatValue];
        BOOL cc = [C boolValue];
    }
    return 0;
}
