//
//  ViewController.m
//  lesson 44_2_pinch
//
//  Created by Yuriy Bosov on 7/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate>
{
    CGFloat lastScaleFactor;
    CGFloat currentRotate;
    CGFloat prevRotate;
    
    CGPoint lastPoint;
}

@property (nonatomic, weak) IBOutlet UIView *rect;

- (void)makeTransform;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    lastScaleFactor = 1;
}

- (IBAction)pich:(UIPinchGestureRecognizer *)sender {
    
    _rect.transform = CGAffineTransformScale(_rect.transform,
                                             sender.scale, sender.scale);
    sender.scale = 1;
    
//    CGAffineTransform rotate = CGAffineTransformMakeRotation(currentRotate);
//    CGAffineTransform scale;
//    
//    if (sender.scale > 1) { // увеличиваем размеры квадрата
//        
//        scale = CGAffineTransformMakeScale(lastScaleFactor + (sender.scale - 1),
//                                           lastScaleFactor + (sender.scale - 1));
//    } else { // уменьшаем размеры
//        scale = CGAffineTransformMakeScale(lastScaleFactor * sender.scale,
//                                           lastScaleFactor * sender.scale);
//    }
//    
//    _rect.transform = CGAffineTransformConcat(scale, rotate);
//    
//    if (sender.state == UIGestureRecognizerStateEnded) {
//        if (sender.scale > 1){
//            lastScaleFactor += (sender.scale - 1);
//        } else {
//            lastScaleFactor *= sender.scale;
//        }
//    }
}

- (IBAction)rotate:(UIRotationGestureRecognizer *)sender {

    _rect.transform = CGAffineTransformRotate(_rect.transform, sender.rotation);
    sender.rotation = 0;
//    CGFloat radians = atan2f(_rect.transform.b, _rect.transform.a);
//    
//    currentRotate = radians + (sender.rotation - prevRotate);
//    prevRotate = sender.rotation;
//    
//    [self makeTransform];
//    
//    if (sender.state == UIGestureRecognizerStateEnded) {
//        prevRotate = 0;
//    }
}

- (IBAction)doubleCliked:(UITapGestureRecognizer *)sender {
    
    if (lastScaleFactor >= 2) {
        lastScaleFactor = 1;
    } else if (lastScaleFactor >= 1.5) {
        lastScaleFactor = 2;
    } else if (lastScaleFactor >= 1.25) {
        lastScaleFactor = 1.5;
    } else if (lastScaleFactor >= 1) {
        lastScaleFactor = 1.25;
    } else {
        lastScaleFactor = 1.0;
    }
    
    [self makeTransform];
}

- (IBAction)longClicked:(UILongPressGestureRecognizer *)sender {
    
    lastScaleFactor = 1;
    currentRotate = 0;
    
    [self makeTransform];
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    
    CGPoint point = [sender translationInView:_rect];
    
    _rect.center = CGPointMake(_rect.center.x + (point.x - lastPoint.x),
                               _rect.center.y + (point.y - lastPoint.y));
    lastPoint = point;
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        lastPoint = CGPointZero;
    }
}

- (void)makeTransform {
    
    CGAffineTransform rotate = CGAffineTransformMakeRotation(currentRotate);
    
    CGAffineTransform scale = CGAffineTransformMakeScale(lastScaleFactor,
                                                         lastScaleFactor);
    
    _rect.transform = CGAffineTransformConcat(rotate, scale);
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (([gestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]] ||
         [gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]) &&
        ([otherGestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]] ||
         [otherGestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]))
    {
        return NO;
    }
    return YES;
}

@end
