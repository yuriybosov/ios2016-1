
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma init

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        dataSource = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 20; i++)
        {
            [dataSource addObject:@(i)];
        }
    }
    return self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    
    NSNumber *num = [dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = [num description];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // шаг 1 - удаляем модель из массива данных
    [dataSource removeObjectAtIndex:indexPath.row];
    
    // шаг 2 - визуально удаляем ячейку
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSNumber *num = [dataSource objectAtIndex:sourceIndexPath.row];
    [dataSource insertObject:num atIndex:destinationIndexPath.row];
    [dataSource removeObjectAtIndex:sourceIndexPath.row];
}

#pragma mark - UITableViewDelegate

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"уДаЛиТь";
}

#pragma mark - Actions

- (IBAction)revomeButtonPressed:(id)sender
{
    // проверяем, а можно ли удалить удалить элемент
    if (dataSource.count > 0)
    {
        // находим индекс пасс последней ячейки
        NSIndexPath *path = [NSIndexPath indexPathForRow:(dataSource.count - 1) inSection:0];
        
        // удаляем последний элемент в массиве данных
        [dataSource removeLastObject];
        
        // визуально удаляем ячейку
        [self.tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (IBAction)addButtonPressed:(id)sender
{
    // Создаем модель, которую хотим добавить
    NSNumber *num = @([[NSDate date] timeIntervalSince1970]);
    
    // добавляем модельку в массив
    [dataSource insertObject:num atIndex:0];
    
    // создаем NSIndexPath
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    
    // визуально добавляем ячейку
    [self.tableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)editButtonPressed:(UIButton *)sender
{
    // вкл\выкл режим редактирование в таблице
    if (self.tableView.isEditing)
    {
        [self.tableView setEditing:NO animated:YES];
        [sender setTitle:@"Edit" forState:UIControlStateNormal];
    }
    else
    {
        [self.tableView setEditing:YES animated:YES];
        [sender setTitle:@"Done" forState:UIControlStateNormal];
    }
}

@end
