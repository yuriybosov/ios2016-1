//
//  CustomClass.h
//  lesson 53_Blocks
//
//  Created by Yuriy Bosov on 9/12/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

// обьявляем тип блок
typedef void(^CustomBlock)(NSString *str, NSInteger i, BOOL b);

@interface CustomClass : NSObject {
    __block NSInteger intValue;
}

// блоки можно объявлять как проперти класса,ОБЯЗАТЕЛЬНО пишем copy!!!
@property (nonatomic, copy) CustomBlock block1;
@property (nonatomic, strong) NSString *name;

- (void)customMethod;
- (void)customMethodWithBlock:(CustomBlock)block;

@end
