//
//  AppDelegate.h
//  lesson_79_1_Localizations2
//
//  Created by Yurii Bosov on 12/5/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

