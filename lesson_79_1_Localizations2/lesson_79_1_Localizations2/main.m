//
//  main.m
//  lesson_79_1_Localizations2
//
//  Created by Yurii Bosov on 12/5/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
