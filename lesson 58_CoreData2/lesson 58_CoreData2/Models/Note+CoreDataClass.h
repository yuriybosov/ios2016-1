//
//  Note+CoreDataClass.h
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Comment;

NS_ASSUME_NONNULL_BEGIN

@interface Note : NSManagedObject

- (NSArray *)commentsList;

@end

NS_ASSUME_NONNULL_END

#import "Note+CoreDataProperties.h"
