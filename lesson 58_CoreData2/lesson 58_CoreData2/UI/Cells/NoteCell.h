//
//  NoteCell.h
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note+CoreDataClass.h"

@interface NoteCell : UITableViewCell

@property (nonatomic, strong) Note *note;

- (void)setupNote:(Note *)note;

@end
