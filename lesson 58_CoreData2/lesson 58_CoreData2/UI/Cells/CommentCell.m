//
//  CommentCell.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CommentCell.h"

@interface CommentCell ()

@property (nonatomic, weak) IBOutlet UILabel *lbNoteName;
@property (nonatomic, weak) IBOutlet UILabel *lbText;
@property (nonatomic, weak) IBOutlet UILabel *lbDate;

@end

@implementation CommentCell

+ (NSDateFormatter *)dateFormatter{
    
    static NSDateFormatter *dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMMM yyyy, HH mm";
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"RU"];
    });
    
    return dateFormatter;
}

- (void)setupComment:(Comment *)comment {
    
    self.comment = comment;
    
    self.lbNoteName.text = comment.note.title;
    self.lbText.text = comment.text;
    self.lbDate.text = [[CommentCell dateFormatter] stringFromDate:comment.createDate];
}

@end
