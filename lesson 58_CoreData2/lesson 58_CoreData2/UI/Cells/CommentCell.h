//
//  CommentCell.h
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Comment+CoreDataClass.h"
#import "Note+CoreDataClass.h"

@interface CommentCell : UITableViewCell

@property (nonatomic, strong) Comment *comment;

- (void)setupComment:(Comment *)comment;

@end
