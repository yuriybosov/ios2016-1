//
//  CommentsController.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CommentsController.h"
#import "CommentCell.h"
#import "CommetnDetailController.h"

@interface CommentsController () {
    NSArray *dataSource;
}

@end

@implementation CommentsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Comments List";
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    dataSource = [self.note commentsList];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
    
    [cell setupComment:[dataSource objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //
    if ([segue.destinationViewController isKindOfClass:[CommetnDetailController class]]) {
        
        ((CommetnDetailController *)segue.destinationViewController).note = self.note;
    }
}

@end
