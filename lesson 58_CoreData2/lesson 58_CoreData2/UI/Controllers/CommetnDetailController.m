//
//  CommetnDetailController.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CommetnDetailController.h"
#import "AppDelegate.h"

@interface CommetnDetailController ()

@property (nonatomic, weak) IBOutlet UITextView *textView;

@end

@implementation CommetnDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Create Commet";
    // Do any additional setup after loading the view.
}

- (IBAction)saveButtonPressed:(id)sender{
    [self.view endEditing:YES];
    
    NSEntityDescription *ed = [NSEntityDescription entityForName:NSStringFromClass([Comment class]) inManagedObjectContext:managedObjectContext()];
    
    Comment *comment = [[Comment alloc] initWithEntity:ed insertIntoManagedObjectContext:self.note.managedObjectContext];
    comment.text = self.textView.text;
    comment.createDate = [NSDate date];
    
    [self.note addCommentsObject:comment];
    
    if (self.note.managedObjectContext != nil) {
        [self.note.managedObjectContext insertObject:comment];
        saveContext();
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
