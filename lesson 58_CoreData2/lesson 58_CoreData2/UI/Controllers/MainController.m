//
//  MainController.m
//  lesson 58_CoreData2
//
//  Created by Yuriy Bosov on 9/23/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MainController.h"
#import "NoteCell.h"
#import "CommentCell.h"
#import "NoteDetailController.h"
#import "CommetnDetailController.h"
#import "AppDelegate.h"

@interface MainController () {
    NSArray *dataSources;
}

@property (nonatomic, weak) IBOutlet UISegmentedControl *segmented;

- (void)showContent;
- (void)showAllNotes;
- (void)showAllComments;

@end

@implementation MainController

- (void)viewWillAppear:(BOOL)animated {
    [self showContent];
}

- (void)showContent {
    if (self.segmented.selectedSegmentIndex == 0) {
        [self showAllNotes];
    } else{
        [self showAllComments];
    }
}

- (void)showAllNotes {
    dataSources = [managedObjectContext() executeFetchRequest:[Note fetchRequest] error:nil];
    [self.tableView reloadData];
}

- (void)showAllComments {
    dataSources = [managedObjectContext() executeFetchRequest:[Comment fetchRequest] error:nil];
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)changeDataSources:(id)sender {
    [self showContent];
}

- (IBAction)trashButtonClicked:(id)sender {
    // очищаем не корректные сущности
    // удаляем все комменты, у которых нет даты создания или нет связи с note
    NSFetchRequest *request = [Comment fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(note == nil) OR (createDate == nil)"];
    request.predicate = predicate;
    NSArray *result = [managedObjectContext() executeFetchRequest:request error:nil];
    for (NSManagedObject *obj in result) {
        [managedObjectContext() deleteObject:obj];
    }
    // сохранили изнимения в бд
    saveContext();
    // обновили экран
    [self showContent];
}

- (IBAction)TEST_ButtonClicked:(id)sender {
    
//    NSFetchRequest *request = [Note fetchRequest];
//    request.predicate = [NSPredicate predicateWithFormat:@"comments.@count >= 5"];
//    NSArray *result = [managedObjectContext() executeFetchRequest:request error:nil];
//    NSLog(@"result %@", result);
    
    NSFetchRequest *request = [Comment fetchRequest];
    request.predicate = [NSPredicate predicateWithFormat:@"text.length > 20"];
    NSArray *result = [managedObjectContext() executeFetchRequest:request error:nil];
    NSLog(@"result %@", result);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if (self.segmented.selectedSegmentIndex == 0){
        
        NoteCell *noteCell = [tableView dequeueReusableCellWithIdentifier:@"NoteCell"];
        [noteCell setupNote:dataSources[indexPath.row]];
        
        cell = noteCell;
        
    } else{
        CommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
        [commentCell setupComment:dataSources[indexPath.row]];
        
        cell = commentCell;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([sender isKindOfClass:[NoteCell class]] &&
        [segue.destinationViewController isKindOfClass:[NoteDetailController class]]) {
        
        NoteCell *cell = sender;
        NoteDetailController *controller = segue.destinationViewController;
        controller.note = cell.note;
    }
}

@end
