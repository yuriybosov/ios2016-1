//
//  main.m
//  lesson_9
//
//  Created by Yuriy Bosov on 2/22/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"
#import "Group.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // шаг 1 - создание студентов
        Student *st1 = [Student creatStudentWithName:@"qwer" withAge:32 withGender:YES];
        Student *st2 = [Student creatStudentWithName:@"asdf" withAge:32 withGender:YES];
        Student *st3 = [Student creatStudentWithName:@"zxcv" withAge:18 withGender:NO];
        
        // шаг 2 - создаем массив из всех студентов
        NSArray *allStudents = @[st1, st2, st3];
        
        // шаг 3 - создаем группу студентов
        Group *iOSGrop = [Group createGroupWithName:@"ios 2016"
                                       withStudents:allStudents];
        
        // шаг 4 - выводим в консоль созданные нами объекты
        NSLog(@"%@", [iOSGrop description]);
        
//        NSLog(@"средний возраст студентов равен %lu", [iOSGrop averageAge]);
//        NSLog(@"кол-во мущин %lu", [iOSGrop countOfMan]);
//        NSLog(@"кол-во женщин %lu", [iOSGrop countOfWoman]);
//        NSLog(@"все имена %@", [iOSGrop allNamesStudents]);
//        NSLog(@"поиск по имени %@", [iOSGrop searchStudentByName:@"yuriy"]);
        
        NSLog(@"сортировка по имени %@", [iOSGrop sortedStudentsByName]);
        NSLog(@"сортировка по возрасту и по имени %@", [iOSGrop sortedStudentsByAgeAndName]);
   }
    return 0;
}
