//
//  Group.h
//  lesson_9
//
//  Created by Yuriy Bosov on 2/22/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

@interface Group : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *students;

+ (Group *)createGroupWithName:(NSString *)name
                  withStudents:(NSArray *)students;

- (NSUInteger)averageAge;
- (NSUInteger)countOfMan;
- (NSUInteger)countOfWoman;
- (NSString *)allNamesStudents;
- (Student *)searchStudentByName:(NSString *)name;

// сортировка по имени
- (NSArray *)sortedStudentsByName;

// сортировка по возрасту и имени
- (NSArray *)sortedStudentsByAgeAndName;

@end
