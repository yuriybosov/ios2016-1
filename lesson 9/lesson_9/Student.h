//
//  Student.h
//  lesson_9
//
//  Created by Yuriy Bosov on 2/22/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject
{
    NSMutableDictionary *journal;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSUInteger age;
@property (nonatomic) BOOL gender;

+ (Student *)creatStudentWithName:(NSString *)name
                          withAge:(NSUInteger)age
                       withGender:(BOOL)gender;

- (void)addRating:(NSUInteger)rating
       forSubject:(NSString *)sabject;

@end
