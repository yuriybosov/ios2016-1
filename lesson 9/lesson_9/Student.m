//
//  Student.m
//  lesson_9
//
//  Created by Yuriy Bosov on 2/22/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Student.h"

@implementation Student

+ (Student *)creatStudentWithName:(NSString *)name
                          withAge:(NSUInteger)age
                       withGender:(BOOL)gender
{
    Student *obj = [[Student alloc] init];
    
    obj.name = name;
    obj.age = age;
    obj.gender = gender;
    
    return obj;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"name: %@, age: %lu, %@", _name, _age, _gender == YES ? @"male" : @"female"];
}

- (void)addRating:(NSUInteger)rating
       forSubject:(NSString *)sabject
{
    
}

@end
