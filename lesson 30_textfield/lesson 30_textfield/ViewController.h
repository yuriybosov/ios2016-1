//
//  ViewController.h
//  lesson 30_textfield
//
//  Created by Yuriy Bosov on 5/16/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
{
    NSArray *inputFields;
}

@property (nonatomic, weak) IBOutlet UITextField *tf1;
@property (nonatomic, weak) IBOutlet UITextField *tf2;
@property (nonatomic, weak) IBOutlet UITextField *tf3;

@end

