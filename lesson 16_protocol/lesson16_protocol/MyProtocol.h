//
//  MyProtocol.h
//  lesson16_protocol
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#ifndef MyProtocol_h
#define MyProtocol_h

// объявление протокола

// протокол - это набор методов, которые будут обязательны или необязательны в реализации
@protocol MyProtocol <NSObject>

@required // обязательны в реализации (по умолчанию всегда @required)

- (void)methodMyProtocolRequired1;
- (void)methodMyProtocolRequired2;

@optional // НЕ обязательны в реализации

- (void)methodMyProtocolOptional1;
- (void)methodMyProtocolOptional2;

@end

#endif /* MyProtocol_h */
