//
//  MyClass.m
//  lesson16_protocol
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "MyClass.h"

@implementation MyClass

- (void)methodByMyClass
{
    // вывод название метода в лог
    NSLog(@"%@", NSStringFromSelector(@selector(methodByMyClass)));
}

#pragma mark - MyProtocol

- (void)methodMyProtocolRequired1
{
    NSLog(@"%@", NSStringFromSelector(@selector(methodMyProtocolRequired1)));
}

- (void)methodMyProtocolRequired2
{
    NSLog(@"%@", NSStringFromSelector(@selector(methodMyProtocolRequired2)));
}

- (void)methodMyProtocolOptional1
{
    NSLog(@"%@", NSStringFromSelector(@selector(methodMyProtocolOptional1)));
}

@end
