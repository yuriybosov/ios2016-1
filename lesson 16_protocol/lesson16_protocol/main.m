//
//  main.m
//  lesson16_protocol
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        MyClass *obj = [[MyClass alloc] init];
        [obj methodByMyClass];
        
        [obj methodMyProtocolRequired1];
        [obj methodMyProtocolRequired2];
        
        // проверка на реализацию метода в классе
        if ([obj respondsToSelector:@selector(methodMyProtocolOptional1)])
        {
            [obj methodMyProtocolOptional1];
        }
        else
        {
            NSLog(@"метод %@ не реализован", NSStringFromSelector(@selector(methodMyProtocolOptional1)));
        }
        
        // проверка на реализацию метода в классе
        if ([obj respondsToSelector:@selector(methodMyProtocolOptional2)])
        {
            [obj methodMyProtocolOptional2];
        }
        else
        {
            NSLog(@"метод %@ не реализован", NSStringFromSelector(@selector(methodMyProtocolOptional2)));
        }
        
        // как можно узнать, что данный класс реализует протокол?
        // нужно у экземпляра класса вызвать метод conformsToProtocol:
        if (
            [obj conformsToProtocol:@protocol(MyProtocol)])
        {
            NSLog(@"%@ реализует протокол %@", NSStringFromClass([obj class]) , NSStringFromProtocol(@protocol(MyProtocol)));
        }
    }
    return 0;
}
