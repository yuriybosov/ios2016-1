//
//  Model.swift
//  lesson_83_Pods
//
//  Created by Vlad on 21.12.16.
//  Copyright © 2016 STEP. All rights reserved.
//

import UIKit

class Model: AnyObject {

    var baseCcy: String = ""
    var buy: String = ""
    var ccy: String = ""
    var sale: String = ""
    
    init(dictionary: [String:Any]) {
        
        self.baseCcy = dictionary["base_ccy"] as! String
        self.buy = dictionary["buy"] as! String
        self.ccy = dictionary["ccy"] as! String
        self.sale = dictionary["sale"] as! String
        
        
        print(self.description())
    }
    
    static func modelsFrom(arrayDictionaries: [[String:Any]]) -> [Model] {
        var models = [Model]()
        
        for dictionary in arrayDictionaries {
            models.append(Model.init(dictionary: dictionary))
        }
        
        return models
    }
    
    func description() -> String {
        
        return String.init(format:"1 \(self.ccy) = %0.2f \(self.baseCcy)", Float(self.buy)!)
    }
    
}
