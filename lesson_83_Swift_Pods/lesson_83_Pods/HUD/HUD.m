//
//  HUD.m
//  lesson 63_CoreLocation
//
//  Created by Yuriy Bosov on 10/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "HUD.h"

@implementation HUD

- (void)showInView:(UIView *)view {
    
    self.backgroundColor = [UIColor clearColor];
    self.hidden = NO;
    self.frame = view.bounds;
    [view addSubview:self];
    
    if (!bgView) {
        bgView = [[UIView alloc] initWithFrame:self.frame];
        bgView.backgroundColor = [UIColor grayColor];
        [self addSubview:bgView];
    }
    
    if (!indicator) {
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        indicator.center = CGPointMake(self.frame.size.width/2,
                                       self.frame.size.height/2);
        [self addSubview:indicator];
    }
    
    [indicator startAnimating];
    bgView.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        bgView.alpha = 0.5;
    }];
}
- (void)hide {
    [indicator stopAnimating];
    [UIView animateWithDuration:0.3 animations:^{
        bgView.alpha = 0;
    }completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
    } ];
}

@end
