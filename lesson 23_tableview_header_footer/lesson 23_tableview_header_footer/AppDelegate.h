//
//  AppDelegate.h
//  lesson 23_tableview_header_footer
//
//  Created by Yuriy Bosov on 4/11/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

