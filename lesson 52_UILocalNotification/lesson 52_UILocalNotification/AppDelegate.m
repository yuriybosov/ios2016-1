//
//  AppDelegate.m
//  lesson 52_UILocalNotification
//
//  Created by Yuriy Bosov on 9/7/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // регистрируем пуши
    // 1. указываем типы уведомлений
    UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge;
    
    // 2. создаем настройки
    UIUserNotificationSettings *mySettings =[UIUserNotificationSettings settingsForTypes:types categories:nil];
    
    // 3. регистрируем
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];

    // если пориложение было запущено через пуш, то в launchOptions будет объект по ключю UIApplicationLaunchOptionsLocalNotificationKey
    UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if (localNotification) {
        [self processingLocalNotification:localNotification withShowAlert:NO];
    }
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    // данный метод вызыватеся при нажатии на локальный пуш (с "рабочего стола" или с центра уведомлений если приложение было "свернуто" или в случае если приложение было активно (пользователь не видет в этом случае ни каких уведомляющих банеров)
    [self processingLocalNotification:notification withShowAlert:YES];
}

- (void)processingLocalNotification:(UILocalNotification *)notification withShowAlert:(BOOL)showAlert{
    
    // обнудляем бадж номер
    // applicationIconBadgeNumber = 0 - над иконкой пропадет число (бадж), удаляются все уведомление нашего приложение в центре уведомлений
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (showAlert) {
        UIAlertController *alertContorller = [UIAlertController alertControllerWithTitle:notification.alertTitle message:notification.alertBody preferredStyle:UIAlertControllerStyleAlert];
        
        [alertContorller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        
        [self.window.rootViewController presentViewController:alertContorller animated:YES completion:nil];
    }

}

@end
