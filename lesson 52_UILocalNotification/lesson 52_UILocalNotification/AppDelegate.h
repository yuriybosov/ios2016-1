//
//  AppDelegate.h
//  lesson 52_UILocalNotification
//
//  Created by Yuriy Bosov on 9/7/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

