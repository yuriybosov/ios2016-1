
#import "Animal.h"

@implementation Animal

+ (Animal *)createObjectWithName:(NSString *)name withAge:(NSUInteger)age withGender:(BOOL)gender
{
    Animal *object = [[Animal alloc] init];
    
    object.name = name;
    object.age = age;
    object.gender = gender;
    
    return object;
}

// ПЕРЕОПРЕДЕЛЕНИЕ МЕТОДА БАЗОВОГО КЛАССА
//- (NSString *)description
//{
//    NSString *result = [NSString stringWithFormat:@"name = %@, age = %lu, gender = %d", _name, _age, _gender];
//    return result;
//}

// РАСШИРЕНИЕ МЕТОДА БАЗОВОГО КЛАССА
- (NSString *)description
{
    NSString *superResult = [super description];
    NSString *strGender = nil;
    
    // <условие> ? <значение если иснита> : <значение если ложь>
 
    // или используем тернарный оператор
    strGender = (_gender == 0) ? @"female" : @"male";
    
    // или используем if else
//    if (_gender == 0)
//    {
//       strGender = @"female";
//    }
//    else
//    {
//        strGender = @"male";
//    }
    
    NSString *result = [NSString stringWithFormat:@"superResult = %@, name = %@, age = %lu, gender = %@", superResult, _name, _age, strGender];
    return result;
}

@end
