
#import <Foundation/Foundation.h>

@interface Animal : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSUInteger age;
@property (nonatomic) BOOL gender;

// статический метод, который возвращает объект класса Animal
// '+' - указывает что метод статический
// '(Animal *)' - указывает что возвращаемый тип - это объект класса Animal

+ (Animal *)createObjectWithName:(NSString *)name
                         withAge:(NSUInteger)age
                      withGender:(BOOL)gender;

@end
