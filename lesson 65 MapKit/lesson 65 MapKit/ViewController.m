//
//  ViewController.m
//  lesson 65 MapKit
//
//  Created by Vlad on 17.10.16.
//  Copyright © 2016 STEP. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "AFNetworking.h"
#import "Place.h"
#import "SVProgressHUD.h"

@interface ViewController () <MKMapViewDelegate> {
    AFHTTPSessionManager *sessionManager;
    NSURLSessionDataTask *currentTask;
}

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"MAP";
    NSURL *url = [NSURL URLWithString:@"https://maps.googleapis.com"];
    sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    [self.mapView setShowsUserLocation:YES];
}

#pragma mark - Button actions

- (IBAction)showDniproCity:(id)sender {
    // отобразить на карте город Днепропетровск
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(48.4638695, 35.04795);
    region.span = MKCoordinateSpanMake(0.05, 0.05);
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)clearAnnotation:(id)sender {
    [self.mapView removeAnnotations:self.mapView.annotations];
}

- (IBAction)longTab:(UILongPressGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        CGPoint point = [recognizer locationInView:self.mapView];
        CLLocationCoordinate2D coordinate = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        
        NSLog(@"%@", NSStringFromCGPoint(CGPointMake(coordinate.latitude, coordinate.longitude)));
        
        // start request
        [self startRequestWithLatitude:coordinate.latitude
                             longitude:coordinate.longitude];
    }
}

#pragma mark - API

- (void)startRequestWithLatitude:(CLLocationDegrees)lat longitude:(CLLocationDegrees)lng {
    
    [self cancelRequest];
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *latlngStr = [NSString stringWithFormat:@"%f,%f", lat, lng];
        NSDictionary *params = @{@"key":@"AIzaSyA95K7QmsKvc0eQgBHllEkg6hPSiIZ26bE",
                                 @"latlng":latlngStr,
                                 @"result_type":@"country|street_address",
                                 @"location_type":@"ROOFTOP",
                                 @"language":@"ru"};
        
        currentTask = [sessionManager GET:@"maps/api/geocode/json"
                               parameters:params
                                 progress:nil
                                  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                       {
                           NSArray *placeRawData = [responseObject objectForKey:@"results"];
                           NSMutableArray *result = [NSMutableArray array];
                           for (NSDictionary *placeData in placeRawData) {
                               Place *place = [Place placeWithData:placeData];
                               [result addObject:place];
                           }
                           
                           [result sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"address" ascending:YES]]];
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [self endRequestWithResponseObject:result
                                                            error:nil];
                           });
                           
                       }
                                  failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                       {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [self endRequestWithResponseObject:nil
                                                            error:error];
                           });
                       }];
    });
}

- (void)cancelRequest {
    if (currentTask && currentTask.state == NSURLSessionTaskStateRunning) {
        [SVProgressHUD dismiss];
        [currentTask cancel];
        currentTask = nil;
    }
}

- (void)endRequestWithResponseObject:(id)responseObject error:(NSError *)error {
    currentTask = nil;
    
    if (error){
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [SVProgressHUD dismissWithDelay:3];
        
    } else if ([responseObject count] == 0){
        [SVProgressHUD showErrorWithStatus:@"Не удалось получить адрес по координатам"];
        [SVProgressHUD dismissWithDelay:3];
    }
    else {
        // TODO show place info
        [SVProgressHUD dismiss];
        Place *place = [responseObject firstObject];

        [self.mapView showAnnotations:@[place] animated:YES];
        [self.mapView selectAnnotation:place animated:YES];
    }
}

#pragma mark - MKMapViewDelegate

//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
//    
//}

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
//    MKAnnotationView *view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"id"];
//    
//    return view;
//}

@end
