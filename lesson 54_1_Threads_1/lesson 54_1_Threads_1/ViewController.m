//
//  ViewController.m
//  lesson 54_1_Threads_1
//
//  Created by Yuriy Bosov on 9/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "CustomThread.h"

@interface ViewController ()
{
    NSMutableArray *array;
    
    NSThread *thr1;
    CustomThread *thr2;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    array = [NSMutableArray array];
}

#pragma mark - Methods

- (void)difficulMethodsInBackground:(id)object {
    
    CGFloat startTime = CACurrentMediaTime();
    
    @synchronized (array) {
        for (NSInteger i = 0; i < 10000000 && ![NSThread currentThread].isCancelled; i++) {
            if (object) {
                [array addObject:object];
            }
        }
    }
    
    
    CGFloat  endTime = CACurrentMediaTime();
    
    // currentThread - возвращает текущий поток
    NSThread *currentThread = [NSThread currentThread];
    NSLog(@"%@ finished %f, array count = %lu", currentThread.name, endTime - startTime, array.count);
    
    if ([currentThread isMainThread]) {
        [self completionMethod];
    } else{
        NSThread *mainThread = [NSThread mainThread];
        [self performSelector:@selector(completionMethod)
                     onThread:mainThread
                   withObject:nil
                waitUntilDone:NO];
    };
}

- (void)completionMethod {
    self.view.backgroundColor = [UIColor greenColor];
    NSLog(@"%@", array);
}

- (void)difficulMethodsInBackgroundGCD:(id)object {

    dispatch_queue_t queue = dispatch_queue_create("com.qwerqwer", DISPATCH_QUEUE_SERIAL);

    dispatch_async(queue, ^{
        
        for (NSInteger i = 0; i < 10000000 && ![NSThread currentThread].isCancelled; i++) {
            if (object) {
                [array addObject:object];
            }
        }
        
        dispatch_queue_t main_queue = dispatch_get_main_queue();
        dispatch_async(main_queue, ^{
            [self completionMethod];
        });
    });
}

#pragma mark - Button Actions

- (IBAction)button1Clicked:(id)sender{
    
    // NSThread - класс для работы с потоками
    thr1 = [[NSThread alloc] initWithTarget:self
                                  selector:@selector(difficulMethodsInBackground:)
                                    object:@"O"];
    thr1.name = @"thread_1";
    [thr1 start];
    
//    NSThread *thr2;
//    thr2 = [[NSThread alloc] initWithTarget:self
//                                  selector:@selector(difficulMethodsInBackground:)
//                                    object:@"X"];
//    thr2.name = @"thread_2";
//    [thr2 start];
}

- (IBAction)stopThreadButtonClicked:(id)sender {
    // остановка потока
    if ([thr1 isExecuting]) {
        NSLog(@"%@ isExecuting", thr1.name);
        
        NSLog(@"%@ need cancel", thr1.name);
        [thr1 cancel];
    }
}

- (IBAction)customThreadStart:(id)sender {
    thr2 = [[CustomThread alloc] init];
    [thr2 start];
}

- (IBAction)customThreadStop:(id)sender {
    if ([thr2 isExecuting]) {
        [thr2 cancel];
    }
}

@end
