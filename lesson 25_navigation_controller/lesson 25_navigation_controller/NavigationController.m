//
//  NavigationController.m
//  lesson 25_navigation_controller
//
//  Created by Yuriy Bosov on 4/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return [self.topViewController supportedInterfaceOrientations];
}

@end
