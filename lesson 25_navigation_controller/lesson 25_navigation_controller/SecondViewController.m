//
//  SecondViewController.m
//  lesson 25_navigation_controller
//
//  Created by Yuriy Bosov on 4/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "SecondViewController.h"
#import "ThirdViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Second Controller";
    
    // создание программно UIBarButtonItem
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self  action:@selector(addButtonPressed:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    //
    self.navigationItem.hidesBackButton = YES;
}

- (IBAction)backButtonPressed:(id)sender
{
    // метод popViewControllerAnimated: "выдаталкивает" текущий экран и выполняется переход на предыдущий экран
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addButtonPressed:(id)sender
{
    // создаем новый View Controller
    ThirdViewController *viewController = [[ThirdViewController alloc] init];
    
    // отобаржаем новый экран (пушим его)
    [self.navigationController pushViewController:viewController animated:YES];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
