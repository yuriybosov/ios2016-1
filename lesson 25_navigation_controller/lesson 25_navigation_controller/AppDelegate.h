//
//  AppDelegate.h
//  lesson 25_navigation_controller
//
//  Created by Yuriy Bosov on 4/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

