//
//  Place.m
//  lesson 64_HTTP_GoogleAPI
//
//  Created by Yuriy Bosov on 10/12/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Place.h"

@implementation Place

+ (Place *)placeWithData:(NSDictionary *)data {
    Place *place = [[Place alloc] init];
    
    id value = [data objectForKey:@"formatted_address"];
    if ([value isKindOfClass:[NSString class]])
        place.address = value;
    
    value = [data objectForKey:@"place_id"];
    if ([value isKindOfClass:[NSString class]])
        place.placeID = value;
    
    
    NSNumber *lat = [data valueForKeyPath:@"geometry.location.lat"];
    NSNumber *lng = [data valueForKeyPath:@"geometry.location.lng"];
    if ([lat isKindOfClass:[NSNumber class]] &&
        [lng isKindOfClass:[NSNumber class]]) {
        
        place.coordinate = CLLocationCoordinate2DMake(lat.doubleValue,
                                                      lng.doubleValue);
        
    }
    return place;
}

@end
