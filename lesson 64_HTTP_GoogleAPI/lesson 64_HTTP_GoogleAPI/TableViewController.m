//
//  TableViewController.m
//  lesson 64_HTTP_GoogleAPI
//
//  Created by Yuriy Bosov on 10/12/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "TableViewController.h"
#import "AFNetworking.h"
#import "Place.h"

@interface TableViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
    NSArray *dataSource;
    AFHTTPSessionManager *sessionManager;
    NSURLSessionDataTask *currentTask;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Поиск места по адресу";
    
    NSURL *url = [NSURL URLWithString:@"https://maps.googleapis.com"];
    sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
}

#pragma mark - API

- (void)startRequestWithSearchText:(NSString *)searchText {
    
    [self cancelRequest];
    
    NSDictionary *params = @{@"key":@"AIzaSyA95K7QmsKvc0eQgBHllEkg6hPSiIZ26bE",
                             @"address":searchText,
                             @"language":@"ru",
                             @"region":@"uah"};
    
    currentTask = [sessionManager GET:@"maps/api/geocode/json"
                           parameters:params
                             progress:nil
                              success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        NSArray *placeRawData = [responseObject objectForKey:@"results"];
        NSMutableArray *result = [NSMutableArray array];
        for (NSDictionary *placeData in placeRawData) {
            Place *place = [Place placeWithData:placeData];
            [result addObject:place];
        }
        
        [result sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"address" ascending:YES]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRequestWithResponseObject:result
                                         error:nil];
        });
        
    }
                              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRequestWithResponseObject:nil
                                         error:error];
        });
    }];
}

- (void)cancelRequest {
    if (currentTask && currentTask.state == NSURLSessionTaskStateRunning) {
        [currentTask cancel];
        currentTask = nil;
    }
}

- (void)endRequestWithResponseObject:(id)responseObject error:(NSError *)error {
    currentTask = nil;
    
    if (error){
        // TODO show error alert
        NSLog(@"error %@", error);
        dataSource = nil;
    } else {
        dataSource = responseObject;
    }
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    Place *place = [dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = place.address;
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //
    if (searchText.length >= 3) {
        
        [self startRequestWithSearchText:searchText];
        
    } else{
        [self cancelRequest];
        dataSource = nil;
        [self.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

@end
