//
//  AppDelegate.m
//  lesson 57_CoreData
//
//  Created by Yuriy Bosov on 9/21/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // for test
    // 1. create new entity
    // 1.1 создали NSEntityDescription
    NSEntityDescription *ed = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:self.persistentContainer.viewContext];
    
    // 1.2 создали NSManagedObject на основе NSEntityDescription
    NSManagedObject *note = [[NSManagedObject alloc] initWithEntity:ed insertIntoManagedObjectContext:self.persistentContainer.viewContext];
    
    // 1.3 используя KVC задаем значения атрибутам
    [note setValue:@"title" forKey:@"title"];
    [note setValue:@"text" forKey:@"text"];
    [note setValue:[NSDate date] forKey:@"createDate"];
    [note setValue:[NSDate date] forKey:@"modifyDate"];
    
    // 1.4 добавление в контекст и сохранение
    [self.persistentContainer.viewContext insertObject:note];
    [self saveContext];
    
    // 2. получить все записи для сущности Note
    // 2.1 создаем NSFetchRequest, указываем для какой именно записи
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Note"];
    // 2.2 создаем NSSortDescriptor, который поможет отсортировать результат
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    // 2.3 выполняем запрос в нашу базу данных
    NSArray *result = [self.persistentContainer.viewContext executeFetchRequest:request error:nil];
    NSLog(@"result %@", result);
    
    NSArray *test = [result valueForKey:@"createDate"];
    NSLog(@"%@", test);
    
    return YES;
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"lesson_57_CoreData"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
