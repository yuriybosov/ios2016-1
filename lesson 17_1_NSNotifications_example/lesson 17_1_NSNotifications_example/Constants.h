//
//  Constants.h
//  lesson 17_1_NSNotifications_example
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString *const kNotificationPrintStart = @"kNotificationPrintStart";
static NSString *const kNotificationPrintEnd = @"kNotificationPrintEnd";
static NSString *const kNotificationPrintFailed = @"kNotificationPrintFailed";


#endif /* Constants_h */
