//
//  main.m
//  lesson 17_1_NSNotifications_example
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Printer.h"
#import "Human.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        Printer *printer = [[Printer alloc] init];
        printer.currentCountPages = 100;
        Human *human = [[Human alloc] init];
        
        NSLog(@"%@", human);
        NSLog(@"%@", printer);
        
        [printer startPrint:110];
    }
    return 0;
}
