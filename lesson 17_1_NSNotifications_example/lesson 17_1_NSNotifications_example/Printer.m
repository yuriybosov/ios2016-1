//
//  Printer.m
//  lesson 17_1_NSNotifications_example
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Printer.h"

@implementation Printer

- (void)startPrint:(NSUInteger)countPages
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    NSDictionary *params = @{@"countPage":@(countPages),
                             @"currentCountPage":@(self.currentCountPages)};
    
    NSString *notificationName = self.currentCountPages >= countPages ? kNotificationPrintStart : kNotificationPrintFailed;
    
    BOOL printing = NO;
    if (self.currentCountPages >= countPages)
    {
        self.currentCountPages -= countPages;
        printing = YES;
    }
    
    [nc postNotificationName:notificationName object:self userInfo:params];
    
    if (printing)
        [self endPrint];
}

- (void)endPrint
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    NSDictionary *params = @{@"currentCountPage":@(self.currentCountPages)};
    NSString *notificationName = kNotificationPrintEnd;
    
    [nc postNotificationName:notificationName object:self userInfo:params];
}

@end
