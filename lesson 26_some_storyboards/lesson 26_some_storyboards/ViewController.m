//
//  ViewController.m
//  lesson 26_some_storyboards
//
//  Created by Yuriy Bosov on 4/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

// задача: отобразить экран, который находится в тоже сторибоарде, что и текщий экран. Новый экран найти в сторибоарде по его id
- (IBAction)openRedViewController:(id)sender
{
    // получаем сторибоард, в котором создан текущий контроллер
    UIStoryboard *storyboard = self.storyboard;
    
    // получаем ViewController из сторибоарда по идентификатору
    UIViewController *redController = [storyboard instantiateViewControllerWithIdentifier:@"redViewController"];
    
    // "пушим" новый контроллер
    [self.navigationController pushViewController:redController animated:YES];
}

- (IBAction)openGreenViewControllerFirst:(id)sender
{
    // создаем новый сторибоард, из которого нужно получить другие экраны
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard2" bundle:nil];
    
    // находим котроллер, на который указывает entry point
    UIViewController *greenControllerFirst = [storyboard instantiateInitialViewController];
    
    // "пушим" новый контроллер
    [self.navigationController pushViewController:greenControllerFirst animated:YES];
}

- (IBAction)openGreenViewControllerSecond:(id)sender
{
    
}

@end
