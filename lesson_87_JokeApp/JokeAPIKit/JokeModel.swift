//
//  JokeModel.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

public class JokeModel: NSObject {
    
    public var site: String?
    public var name: String?
    public var desc: String?
    public var link: String?
    public var elementPureHtml: String?
    
   
    
    
    public init(dict:[String:Any]) {
        if let site = dict["site"] as? String {
            self.site = site
        }
        if let name = dict["name"] as? String {
            self.name = name
        }
        if let desc = dict["desc"] as? String {
            self.desc = desc
        }
        if let link = dict["link"] as? String {
            self.link = link
        }
        if let elementPureHtml = dict["elementPureHtml"] as? String {
            self.elementPureHtml = elementPureHtml
        }
    }
    
    public func shareURL() -> URL {
        return URL.init(string:"http://" + self.site! + self.link!)!
    }
    
    public static func loadJokes(forCategoty: JokeCategoryModel?, count: Int, complition:@escaping(_ data: [JokeModel]?, _ errorMessage: String?)->()) {
        
        var url: URL!
        
        if let category = forCategoty as JokeCategoryModel! {
            let str = String("http://www.umori.li/api/get?site=\(category.site!)&name=\((category.name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))!)&num=\(count)")
            url = URL(string: str!)
        } else {
            url = URL(string:"http://www.umori.li/api/get?site=bash.im&name=bash&num=\(count)")
        }
        
        print(url)
        
        let task = URLSession.shared.dataTask(with: url!) {(data: Any?, responce: URLResponse?, error: Error?) in

            if let rawData = data as? Data {
                
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: rawData, options: .allowFragments) as! [[String:Any]]
                    
                    var jokes = [JokeModel]()
                    
                    for object in jsonData {
                        jokes.append(JokeModel.init(dict: object))
                    }
                    
                    DispatchQueue.main.async {
                        complition(jokes, nil)
                    }
                    
                } catch {
                    DispatchQueue.main.async {
                        complition(nil, "Ошибка парсера!!!")
                    }
                }
            } else {
                DispatchQueue.main.async {
                    complition(nil, error?.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    public override var description: String {
        return self.name!+" "+self.desc!;
    }
    
    public func jokeAttributedText() -> NSAttributedString {
        
        var attributeText: NSAttributedString
        
        let htmlData = self.elementPureHtml?.data(using: String.Encoding.unicode)
        
        do {
            attributeText = try NSAttributedString(data: htmlData!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType], documentAttributes: nil)
        } catch {
            attributeText = NSAttributedString.init(string: self.elementPureHtml!)
        }
        return attributeText
    }
}
