//
//  JokeCategoryModel.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit

public class JokeCategoryModel: NSObject, NSCoding {

    public var site: String?
    public var name: String?
    public var url: String?
    public var parsel: String?
    public var encoding: String?
    public var linkpar: String?
    public var desc: String?
    
    public init(dict:[String:String]) {
        self.site = dict["site"]
        self.name = dict["name"]
        self.url = dict["url"]
        self.parsel = dict["parsel"]
        self.encoding = dict["encoding"]
        self.linkpar = dict["linkpar"]
        self.desc = dict["desc"]
    }
    
    public static func loadJokesCategories(complition: @escaping(_ data: [JokeCategoryModel]?, _ errorMessage: String?) ->()) {
        let url = URL(string:"http://www.umori.li/api/sources")
        
        let task = URLSession.shared.dataTask(with: url!) {(data: Any?, responce: URLResponse?, error: Error?) in
            
            if let rawData = data as? Data {
                
                do {
                    
                    let jsonData = try JSONSerialization.jsonObject(with: rawData, options: .allowFragments) as! [[[String:String]]]
                    
                    var categories = [JokeCategoryModel]()
                    
                    for array in jsonData {
                        for dict in array {
                            categories.append(JokeCategoryModel.init(dict: dict))
                        }
                    }
                    
                    DispatchQueue.main.async {
                        complition(categories, nil)
                    }
                    
                } catch {
                    DispatchQueue.main.async {
                        complition(nil, "Ошибка парсера!!!")
                    }
                }
                
            } else {
                DispatchQueue.main.async {
                    complition(nil, error?.localizedDescription)
                }
            }
        }
        task.resume()
    }
    
    // MARK: - NSCoding
    public required init?(coder aDecoder: NSCoder) {
        self.site = aDecoder.decodeObject(forKey: "site") as! String?
        self.name = aDecoder.decodeObject(forKey: "name") as! String?
        self.url = aDecoder.decodeObject(forKey: "url") as! String?
        self.parsel = aDecoder.decodeObject(forKey: "parsel") as! String?
        self.encoding = aDecoder.decodeObject(forKey: "encoding") as! String?
        self.linkpar = aDecoder.decodeObject(forKey: "linkpar") as! String?
        self.desc = aDecoder.decodeObject(forKey: "desc") as! String?
        
    }
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.site, forKey: "site")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.url, forKey: "url")
        aCoder.encode(self.parsel, forKey: "parsel")
        aCoder.encode(self.encoding, forKey: "encoding")
        aCoder.encode(self.linkpar, forKey: "linkpar")
        aCoder.encode(self.desc, forKey: "desc")
    }
}
