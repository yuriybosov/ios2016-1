//
//  SettingsController.swift
//  lesson_87_JokeApp
//
//  Created by Yurii Bosov on 1/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import JokeAPIKit

class SettingsController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var dataSources = [JokeCategoryModel]()
    var selectedCategoty: JokeCategoryModel?
    
    @IBOutlet weak var offlineSwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = UserDefaults.standard.object(forKey: "category") as! Data! {
            self.selectedCategoty = NSKeyedUnarchiver.unarchiveObject(with: data) as! JokeCategoryModel?
        }
        
        self.activity.startAnimating()
        
        JokeCategoryModel.loadJokesCategories { (data:[JokeCategoryModel]?, error: String?) in
            self.activity.stopAnimating()
            
            if let dataModels = data as [JokeCategoryModel]! {
                
                self.dataSources.append(contentsOf: dataModels)
                self.tableView.reloadData()
                
            } else {
                // show error aletr
                let alert = UIAlertController.init(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func offlineSwitchChangeValue(sender: UISwitch) {
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        
        cell.textLabel?.text = dataSources[indexPath.row].desc
        cell.detailTextLabel?.text = dataSources[indexPath.row].site
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if self.selectedCategoty != nil &&
            dataSources[indexPath.row].desc!+dataSources[indexPath.row].name! == (self.selectedCategoty?.desc)!+(self.selectedCategoty?.name)! {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) as UITableViewCell! {
            
            if cell.accessoryType != UITableViewCellAccessoryType.checkmark {
                
                self.selectedCategoty = dataSources[indexPath.row]
                self.tableView .reloadData();
                
                // save categoty to user default
                let data = NSKeyedArchiver.archivedData(withRootObject: self.selectedCategoty!)
                UserDefaults.standard.set(data, forKey: "category")
                
                // post notification with change category
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeCategory"), object: self.selectedCategoty)
            }
        }
    }
}
