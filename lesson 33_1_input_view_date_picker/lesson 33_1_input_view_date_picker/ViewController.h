//
//  ViewController.h
//  lesson 33_1_input_view_date_picker
//
//  Created by Yuriy Bosov on 5/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
{
    NSDate *dateOfBirth;
    NSDate *morningTime;
    NSDate *nightTime;
    
    NSDateFormatter *dateFormater;
    NSDateFormatter *timeFormater;
    
    IBOutlet UITextField *tfDateOfBirth;
    IBOutlet UITextField *tfMorningTime;
    IBOutlet UITextField *tfNightTime;
    
    UIDatePicker *datePicker;
}

@end

