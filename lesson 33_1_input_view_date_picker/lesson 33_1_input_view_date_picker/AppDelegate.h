//
//  AppDelegate.h
//  lesson 33_1_input_view_date_picker
//
//  Created by Yuriy Bosov on 5/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

