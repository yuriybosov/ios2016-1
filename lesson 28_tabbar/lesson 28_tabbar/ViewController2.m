//
//  ViewController2.m
//  lesson 28_tabbar
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController2.h"

@implementation ViewController2

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.title = @"VC2";
        
        self.tabBarItem.image = [UIImage imageNamed:@"apple"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"apple_filled"];
    }
    return self;
}

@end
