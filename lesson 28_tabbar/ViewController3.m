//
//  ViewController3.m
//  lesson 28_tabbar
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController3.h"

@implementation ViewController3

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.title = @"VC3";
        
        self.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemHistory tag:0];
    }
    return self;
}

@end
