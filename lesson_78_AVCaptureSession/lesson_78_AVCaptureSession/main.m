//
//  main.m
//  lesson_78_AVCaptureSession
//
//  Created by Yurii Bosov on 11/30/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
