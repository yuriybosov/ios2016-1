//
//  ViewController.m
//  lesson_78_AVCaptureSession
//
//  Created by Yurii Bosov on 11/30/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController () <AVCaptureFileOutputRecordingDelegate> {
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *captureLayer;
    AVCaptureMovieFileOutput *output;
    
    IBOutlet UIView *previewVideoView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    session = [[AVCaptureSession alloc] init];
    
    //
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    
    //
    NSError *error1 = nil;
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error1];
    
    NSError *error2 = nil;
    AVCaptureDeviceInput * audioInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error2];
    
    if (!error1 && !error2 && videoInput && audioInput) {
        
        //
        [session addInput:videoInput];
        [session addInput:audioInput];

        //
        output = [[AVCaptureMovieFileOutput alloc] init];
        [session addOutput:output];
        
        //
        captureLayer = [[AVCaptureVideoPreviewLayer alloc]initWithSession:session];
        [captureLayer setVideoGravity:AVLayerVideoGravityResize];
        [previewVideoView.layer addSublayer:captureLayer];
        
        [session startRunning];
    }
    else {
        NSLog(@"error1 %@", error1);
        NSLog(@"error2 %@", error2);
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    captureLayer.frame = previewVideoView.bounds;
}

#pragma mark - Action

- (IBAction)startRecording:(id)sender {
    
    if (!output.isRecording) {
        // старт записи
        // нужно указать путь куда сохранять файл
        
        NSString *fileName = [NSString stringWithFormat:@"%0.f.mp4", [[NSDate date] timeIntervalSince1970]];
        NSString *cacheDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
        NSString *fullPath = [cacheDirectory stringByAppendingPathComponent:fileName];
        NSURL *url = [NSURL fileURLWithPath:fullPath];
        
        [output startRecordingToOutputFileURL:url
                            recordingDelegate:self];
    }
}

- (IBAction)stopRecording:(id)sender {
    [output stopRecording];
    
    NSLog(@"metadata %@",[output metadata]);
//    NSLog(@"availableVideoCodecTypes %@",[output availableVideoCodecTypes]);
}

#pragma mark - AVCaptureFileOutputRecordingDelegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error {
    
    NSLog(@"error %@", error);
    NSLog(@"outputFileURL %@", outputFileURL);
}

@end
