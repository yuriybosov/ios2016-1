//
//  ViewController.m
//  lesson 51_UIImagePickerController
//
//  Created by Yuriy Bosov on 9/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

- (IBAction)takePhotoButtonClicked:(UIBarButtonItem *)sender;
- (void)showImagePickerWithType:(UIImagePickerControllerSourceType)type;

@end

@implementation ViewController

#pragma mark - Button Actions

- (IBAction)takePhotoButtonClicked:(UIBarButtonItem *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Action" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    __weak ViewController *weakSelf = self;
    
    // проверили наличие камеры на девайсе
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            // camera
            [weakSelf showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
            
        }];
        [alert addAction:takePhotoAction];
    }
    
    UIAlertAction *choosePhotoAction = [UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // photo librar
        [weakSelf showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
        
    }];
    [alert addAction:choosePhotoAction];
    
    // if iphone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        // add cancel button
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        // отображаем его в поповере!!!
        UIPopoverPresentationController *popover = [alert popoverPresentationController];
        popover.barButtonItem = sender;
        alert.modalPresentationStyle = UIModalPresentationPopover;
    }
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)showImagePickerWithType:(UIImagePickerControllerSourceType)type {
    
    UIImagePickerController *pickerConrtoller = [[UIImagePickerController alloc] init];
    pickerConrtoller.delegate = self;
    pickerConrtoller.sourceType = type;
    [self.navigationController presentViewController:pickerConrtoller animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *photo = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    //если фото получено из камеры - то сохораняем его в галерею
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        UIImageWriteToSavedPhotosAlbum(photo, self, nil, nil);
        
    }
    self.imageView.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSLog(@"%@", info);
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
