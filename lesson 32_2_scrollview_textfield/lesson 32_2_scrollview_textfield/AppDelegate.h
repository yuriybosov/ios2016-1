//
//  AppDelegate.h
//  lesson 32_2_scrollview_textfield
//
//  Created by Yuriy Bosov on 5/23/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

