//
//  ViewController.m
//  lesson 32_2_scrollview_textfield
//
//  Created by Yuriy Bosov on 5/23/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

- (void)test:(UITextField *)tf;

@end

@implementation ViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self test:textField];
}

#pragma mark - Notifications

- (void)keyboardWillHide:(NSNotification *)notification
{
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    CGRect rect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, rect.size.height, 0);
    
    
    for (UITextField *tf in scrollView.subviews)
    {
        if ([tf isKindOfClass:[UITextField class]] &&
            [tf isFirstResponder])
        {
            [self test:tf];
            break;
        }
    }
    
}

- (void)test:(UITextField *)textField
{
    CGFloat center = (scrollView.frame.size.height - scrollView.contentInset.bottom)/2;
    if (textField.frame.origin.y < center)
    {
        [scrollView setContentOffset:CGPointZero
                            animated:YES];
    }
    else
    {
        [scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y - center) animated:YES];
    }
}

@end
