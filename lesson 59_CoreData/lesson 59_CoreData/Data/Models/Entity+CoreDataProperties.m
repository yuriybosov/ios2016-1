//
//  Entity+CoreDataProperties.m
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Entity+CoreDataProperties.h"

@implementation Entity (CoreDataProperties)

+ (NSFetchRequest<Entity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
}

@dynamic title;
@dynamic valueInt;

@end
