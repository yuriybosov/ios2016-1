//
//  Entity+CoreDataClass.h
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Entity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Entity+CoreDataProperties.h"
