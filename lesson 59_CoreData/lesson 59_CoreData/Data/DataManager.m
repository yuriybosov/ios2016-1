//
//  DataManager.m
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+ (DataManager *)sharedManager {
    static DataManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        // инициализация persistentContainer
        persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Model"];
        [persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error)
        {
            if (error != nil) {
                NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                abort();
            }
        }];
        
        // если в базе данных нет сущностей - создаем их
        NSFetchRequest *request = [Entity fetchRequest];
        request.fetchLimit = 1;
        NSArray *result = [[self context] executeFetchRequest:request error:nil];
        if (result.count == 0) {
            for (NSInteger i = 0; i < 100; i++) {
                NSEntityDescription *ed = [NSEntityDescription entityForName:NSStringFromClass([Entity class]) inManagedObjectContext:[self context]];
                
                Entity *entity = [[Entity alloc] initWithEntity:ed insertIntoManagedObjectContext:[self context]];
                entity.title = [NSString stringWithFormat:@"%li",i + 1];
                entity.valueInt = @(i);
            }
            [self saveContext];
        }
        
    }
    return self;
}

- (NSManagedObjectContext *)context {
    return persistentContainer.viewContext;
}

- (void)saveContext {
    NSError *error = nil;
    
    if ([[self context] hasChanges] && ![[self context] save:&error]) {
        
        NSLog(@"DB save error %@", error);
        abort();
        
    }
}

@end
