//
//  ViewController.m
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "LoadMoreCell.h"

#define count_of_page 10

@interface ViewController () {
    NSMutableArray *dataSources;
    NSFetchRequest *fetchRequest;
    
    BOOL canLoadNewData;
}

- (void)loadDataFromOffset:(NSUInteger)offset needClearCurrentData:(BOOL)clearData;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // инитим fetchRequest
    fetchRequest = [Entity fetchRequest];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"valueInt" ascending:YES]];
    fetchRequest.fetchLimit = count_of_page;
    fetchRequest.fetchOffset = 0;
    
    // инитим массив (выделяем ему память)
    dataSources = [NSMutableArray new];
    
    // добавляем pull to refresh controll
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    // загружаем данные
    [self loadDataFromOffset:0 needClearCurrentData:NO];
}

- (void)refreshData {
    
    // загружаем данные
    [self loadDataFromOffset:0 needClearCurrentData:YES];
    [self.refreshControl endRefreshing];
}

- (void)loadDataFromOffset:(NSUInteger)offset needClearCurrentData:(BOOL)clearData {
    
    fetchRequest.fetchOffset = offset;
    NSArray *result = [[DataManager sharedManager].context executeFetchRequest:fetchRequest error:nil];
    if (clearData) {
        [dataSources removeAllObjects];
    }
    canLoadNewData = (result.count == count_of_page);
    [dataSources addObjectsFromArray:result];
    [self.tableView reloadData];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count + (canLoadNewData ? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = (indexPath.row == dataSources.count) ? @"loadMoreCellID" : @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (indexPath.row < dataSources.count) {
        Entity *entity = [dataSources objectAtIndex:indexPath.row];
        cell.textLabel.text = entity.title;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // проверяем, а не последняя ли у нас ячейка
    if ([cell isKindOfClass:[LoadMoreCell class]]) {
        // если да, то подгрузим новые данные
        [((LoadMoreCell *)cell).activity startAnimating];
        
        [self loadDataFromOffset:fetchRequest.fetchOffset + count_of_page needClearCurrentData:NO];
    }
}

@end
