//
//  LoadMoreCell.m
//  lesson 59_CoreData
//
//  Created by Yuriy Bosov on 9/28/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "LoadMoreCell.h"

@implementation LoadMoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
