//
//  ViewController.h
//  lesson 21_1_custom_cell
//
//  Created by Yuriy Bosov on 4/4/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

