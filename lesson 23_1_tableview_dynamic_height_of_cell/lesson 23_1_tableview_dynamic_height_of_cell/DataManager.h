//
//  DataManager.h
//  lesson 21_1_custom_cell
//
//  Created by Yuriy Bosov on 4/6/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject
{
    NSMutableArray *newsList;
}

+ (DataManager *)sharedInstance;

- (NSArray *)newsList;

@end
