//
//  NewsDetailedController.m
//  lesson 23_1_tableview_dynamic_height_of_cell
//
//  Created by Yuriy Bosov on 4/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "NewsDetailedController.h"

@interface NewsDetailedController ()

@end

@implementation NewsDetailedController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.news.title;
    
    self.newsLogo.image = [UIImage imageNamed:self.news.logo];
    self.newsText.text = self.news.text;
}

@end
