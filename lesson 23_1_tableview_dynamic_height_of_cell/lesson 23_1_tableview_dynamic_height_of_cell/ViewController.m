#import "ViewController.h"
#import "NewsCell.h"
#import "DataManager.h"
#import "NewsDetailedController.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"News List";
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[DataManager sharedInstance] newsList].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newsCell"];
    
    News *news = [[[DataManager sharedInstance] newsList] objectAtIndex:indexPath.row];
    [cell setupNews:news];
    
    return cell;
}

#pragma mark - UITableViewDelegate

// для реализации динамический ячеек нужно корректно настроить аутолаяуты и переопределить метод tableView:estimatedHeightForRowAtIndexPath, в котором мернуть в качестве высоты флаг UITableViewAutomaticDimension
// а метод tableView:heightForRowAtIndexPath: нужно вообще убрать
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

 -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Rotation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - Navigation

// данный метод вызовется при любом переходе на новый экран, при условии что этот переход был настроен в сторибоарде
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // добавляем проверку, что переход был вызван нажатием на ячейку с новостью. Для этого проверим каким классом является объект sender
    if ([sender isKindOfClass:[NewsCell class]])
    {
        NewsCell *cell = sender;
        NewsDetailedController *detailedController = segue.destinationViewController;
        
        detailedController.news = cell.newsObject;
    }
}

@end
