//
//  NewsDetailedController.h
//  lesson 23_1_tableview_dynamic_height_of_cell
//
//  Created by Yuriy Bosov on 4/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"

@interface NewsDetailedController : UIViewController

@property (nonatomic, strong) News* news;

@property (nonatomic, weak) IBOutlet UIImageView *newsLogo;
@property (nonatomic, weak) IBOutlet UILabel *newsText;

@end
