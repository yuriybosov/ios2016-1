//
//  Contact.h
//  lesson 15_repeat_oop
//
//  Created by Yuriy Bosov on 3/16/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
{
    NSMutableArray *phones;
}

@property (nonatomic, strong) NSString *name;

+ (Contact *)crateContactWithName:(NSString *)name
                            phone:(NSString *)phone;

- (void)addPhone:(NSString *)phone;
- (void)removePhone:(NSString *)phone;
- (NSArray *)phones;

@end
