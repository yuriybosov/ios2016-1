//
//  Contact.m
//  lesson 15_repeat_oop
//
//  Created by Yuriy Bosov on 3/16/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        phones = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (Contact *)crateContactWithName:(NSString *)name phone:(NSString *)phone
{
    Contact *contact = [[Contact alloc] init];
    
    contact.name = name;
    [contact addPhone:phone];
    
    return contact;
}

- (void)addPhone:(NSString *)phone
{
    if (![phones containsObject:phone])
    {
        [phones addObject:phone];
    }
}

- (void)removePhone:(NSString *)phone
{
    [phones removeObject:phone];
}

- (NSArray *)phones
{
    return phones;
}

@end
