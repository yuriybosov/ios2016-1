//
//  ViewController.m
//  lesson 23_2_autolayout
//
//  Created by Yuriy Bosov on 4/11/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
