//
//  Worker.h
//  lesson 7
//
//  Created by Yuriy Bosov on 2/15/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Human.h"

@interface Worker : Human

@property (nonatomic, strong) NSString *placeWork;
@property (nonatomic, assign) NSUInteger sallary;
@property (nonatomic, assign) NSUInteger retireAge;

+ (id)createObjectWithName:(NSString *)name
                   withAge:(NSUInteger)age
                withGender:(BOOL)gender
             withPlaceWork:(NSString *)placeWork
               withSallary:(NSUInteger)sallary
             withRetireAge:(NSUInteger)retireAge;


@end
