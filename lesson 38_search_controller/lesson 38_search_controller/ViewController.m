//
//  ViewController.m
//  lesson 38_search_controller
//
//  Created by Yuriy Bosov on 6/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "ResultTableViewController.h"

@interface ViewController () <UISearchResultsUpdating, ResultTableViewProtocol>

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma  mark - Actions

- (IBAction)searchButtonClicked:(UIBarButtonItem *)sender {
    
    ResultTableViewController *resultVC = [self.storyboard instantiateViewControllerWithIdentifier:@"resultVC"];
    resultVC.delegate = self;
    UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:resultVC];
    searchController.searchResultsUpdater = self;

    [self presentViewController:searchController animated:YES completion:nil];
}


#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSLog(@"searchText = %@", searchController.searchBar.text);
    
    NSArray *newDataSource = @[@"qwer",@"asdf",@"zxvc",@"zxcv",@"qwer",@"asdf",@"zxvc",@"zxcv",@"qwer",@"asdf",@"zxvc",@"zxcv"];
    
    if ([searchController.searchResultsController isKindOfClass:[ResultTableViewController class]]) {
        
        [(ResultTableViewController *)searchController.searchResultsController reloadDataSource:newDataSource];
        
    }
}

#pragma mark - ResultTableViewProtocol

- (void)resultTableViewController:(ResultTableViewController *)resultViewController didSelectItem:(id)item {
    
    self.label.text = item;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
