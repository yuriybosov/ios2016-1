//
//  ResultTableViewController.m
//  lesson 38_search_controller
//
//  Created by Yuriy Bosov on 6/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ResultTableViewController.h"

@interface ResultTableViewController ()
{
    NSArray *dataSource;
}

@end

@implementation ResultTableViewController

- (void)reloadDataSource:(NSArray *)aDataSource {
    dataSource = aDataSource;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [dataSource objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate resultTableViewController:self didSelectItem:[dataSource objectAtIndex:indexPath.row]];
}

@end
