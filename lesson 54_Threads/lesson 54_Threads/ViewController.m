//
//  ViewController.m
//  lesson 54_Threads
//
//  Created by Yuriy Bosov on 9/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *array;
}

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    array = [NSMutableArray array];
}

#pragma mark - Methods

- (void)difficulMethodsInBackground:(id)object {
    
    CGFloat startTime = CACurrentMediaTime();
    
    // c помощью @synchronized мы блокируем выполнение кода с разных потоков.
    // если нескольок потоков вызывают данный код, то сначала отработает в том потоке, который "пришел" перый, остальные потоки будут ожидать, пока первый осовбодит этот код
//    @synchronized(self) {
        for (NSInteger i = 0; i < 1000; i++) {
            [array addObject:object];
//        }
    };
    
    CGFloat  endTime = CACurrentMediaTime();
    NSLog(@"finish %f", endTime - startTime);
    
    // выполняем метод в главном потоке
    [self performSelectorOnMainThread:@selector(completionMethod) withObject:nil waitUntilDone:NO];
}

- (void)completionMethod {
    self.view.backgroundColor = [UIColor greenColor];
    NSLog(@"%@", array);
}

#pragma mark - Button Actions

- (IBAction)button1Clicked:(id)sender{
    
    // выполняем метод в беграунде
    [self performSelectorInBackground:@selector(difficulMethodsInBackground:) withObject:@"O"];
    [self performSelectorInBackground:@selector(difficulMethodsInBackground:) withObject:@"X"];
    [self performSelectorInBackground:@selector(difficulMethodsInBackground:) withObject:@"Y"];
}

@end
