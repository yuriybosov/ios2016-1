//
//  AppDelegate.h
//  lessom 35_1_activity_indicator
//
//  Created by Yuriy Bosov on 6/1/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

