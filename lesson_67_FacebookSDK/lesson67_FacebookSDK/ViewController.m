//
//  ViewController.m
//  lesson67_FacebookSDK
//
//  Created by Yurii Bosov on 10/24/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>


@interface ViewController () <FBSDKAppInviteDialogDelegate> {
    FBSDKLoginManager *loginManager;
    
    IBOutlet UIView *contentView;
    IBOutlet UIButton *loginButton;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    loginManager = [[FBSDKLoginManager alloc] init];
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"email"] &&
        [[FBSDKAccessToken currentAccessToken] hasGranted:@"user_friends"]) {
        loginButton.hidden = YES;
    } else {
        contentView.hidden = YES;
    }
}

#pragma mark - Buttons Actions

- (IBAction)loginButtonClicked:(id)sender{
    
    [loginManager logInWithReadPermissions:@[@"email",@"user_friends",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (result.token){
            // hide login button
            // show other buttons
            loginButton.hidden = YES;
            contentView.hidden = NO;
        } else {
            // show error alert
            NSLog(@"\nlogin isCancelled = %i\nlogin error %@", [result isCancelled], error);
        }
    }];
}

- (IBAction)logoutButtonClicked:(id)sender{
    [loginManager logOut];
    
    loginButton.hidden = NO;
    contentView.hidden = YES;
}

- (IBAction)inviteButtonClicked:(id)sender{
    // use invite dialog
    
    FBSDKAppInviteContent *content = [[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/932499060188273"];
    
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:self];
}

- (IBAction)postButtonClicked:(id)sender{
    // use post\share dialog
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:@"http://www.liga.net/"];
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
}

#pragma mark - FBSDKAppInviteDialogDelegate

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    ;
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    ;
}

@end

