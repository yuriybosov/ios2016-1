//
//  AppDelegate.h
//  lesson67_FacebookSDK
//
//  Created by Yurii Bosov on 10/24/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

