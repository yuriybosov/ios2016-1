//
//  AppDelegate.h
//  lesson 27_dynamic_header_footer_view
//
//  Created by Yuriy Bosov on 4/20/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

