//
//  ViewController.h
//  lesson 27_dynamic_header_footer_view
//
//  Created by Yuriy Bosov on 4/20/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *footerView;

@property (nonatomic, strong) IBOutlet UIImageView *headerLogo;
@property (nonatomic, strong) IBOutlet UILabel *headerText;
@property (nonatomic, strong) IBOutlet UILabel *footerText;

- (void)resizeHeaderViewForWidht:(CGFloat)widht;
- (void)resizeFooterViewForWidht:(CGFloat)widht;

@end

