//
//  ViewController.m
//  lessom 29_animations
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction)startButtonPressd:(id)sender
{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self moveToRight];
}

#pragma mark - Animations

- (void)moveToRight
{
    CGRect frame = self.rect.frame;
    frame.origin.x = self.view.frame.size.width - self.rect.frame.size.width;
    
    __weak ViewController *weakSelf = self;
    
    [UIView animateWithDuration:2 animations:^{
        weakSelf.rect.frame = frame;
    } completion:^(BOOL finished) {
        [weakSelf moveToLeft];
    }];
}

- (void)moveToLeft
{
    CGRect frame = self.rect.frame;
    frame.origin.x = 0;
    
    __weak ViewController *weakSelf = self;
    
    [UIView animateWithDuration:2 animations:^{
        weakSelf.rect.frame = frame;
    } completion:^(BOOL finished) {
        [weakSelf moveToRight];
    }];
}

@end
