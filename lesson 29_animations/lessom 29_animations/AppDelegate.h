//
//  AppDelegate.h
//  lessom 29_animations
//
//  Created by Yuriy Bosov on 5/11/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

