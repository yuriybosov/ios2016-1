//
//  Truck.m
//  lesson 16_1_protocol_example
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Truck.h"

@implementation Truck

- (void)prepareToMove
{
    self.destination = [self.dataSourceDelegate destinationOfTruck:self];
    self.typeCargo = [self.dataSourceDelegate typeCargoOfTruck:self];
}

- (void)startMove
{
    NSLog(@"грузовик %@ начал движение в %@, перевозит %@", self.truckName, self.destination, [self typeCargoToString:self.typeCargo]);
}

- (void)endMove
{
    NSLog(@"грузовик %@ приехал в %@, привез %@", self.truckName, self.destination, [self typeCargoToString:self.typeCargo]);
    self.typeCargo = cargoEmpty;
}

- (NSString *)typeCargoToString:(Cargo)cargo
{
    NSString *result = nil;
    switch (cargo)
    {
        case cargoContainer:
            result = @"контейнер";
            break;
            
        case cargoForest:
            result = @"лес";
            break;
            
        case cargoAutoTrailer:
            result = @"автомобили";
            break;
            
        default:
            result = @"пустой груз";
            break;
    }
    
    return result;
}

@end
