//
//  TruckDataSourceProtocol.h
//  lesson 16_1_protocol_example
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#ifndef TruckDataSourceProtocol_h
#define TruckDataSourceProtocol_h

#import <Foundation/Foundation.h>

@class Truck;

// создание enum для определения типа перевозимого груза
typedef enum : NSUInteger {
    cargoEmpty,             //пустой, тип не задан      0
    cargoContainer,         //перевозка контейнеров     1
    cargoForest,            //перевозка леса            2
    cargoAutoTrailer        //перевозка автомобилей     3
} Cargo;

// протокол наполнение данными грузовика
@protocol TruckDataSourceProtocol <NSObject>

- (NSString *)destinationOfTruck:(Truck*)aTruck;    // метод возвращает пункт назначения
- (Cargo)typeCargoOfTruck:(Truck*)aTruck;           // метод возвращает тип перевозимого груза

@end

#endif /* TruckDataSourceProtocol_h */
