//
//  Dispatcher.h
//  lesson 16_1_protocol_example
//
//  Created by Yuriy Bosov on 3/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TruckDataSourceProtocol.h"

@interface Dispatcher : NSObject <TruckDataSourceProtocol>

@end
