
#import <Foundation/Foundation.h>
#import "Tigr.h"
#import "Elephant.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        Tigr *tiger = [Tigr createObjectWithName:@"vasya" withAge:7 withGender:YES];
        Elephant *elephant = [Elephant createObjectWithName:@"petya" withAge:22 withGender:NO];
        
        NSLog(@"%@", [tiger description]);
        NSLog(@"%@", [elephant description]);
    }
    return 0;
}
