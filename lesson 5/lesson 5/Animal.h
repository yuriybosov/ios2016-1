
#import <Foundation/Foundation.h>

@interface Animal : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSUInteger age;
@property (nonatomic) BOOL gender;

+ (id)createObjectWithName:(NSString *)name
                   withAge:(NSUInteger)age
                withGender:(BOOL)gender;

- (BOOL)isYoung;

@end
