
#import "Tigr.h"

@implementation Tigr

//
- (BOOL)isYoung
{
    BOOL value = (self.age <= 10) ? YES : NO;
    return value;
}

//
- (NSString *)description
{
    NSString *superDescription = [super description];
    return [NSString stringWithFormat:@"Tiger, %@", superDescription];
}

@end
