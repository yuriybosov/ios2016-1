//
//  GameFiled.h
//  lesson 48_game_sapper
//
//  Created by Yuriy Bosov on 7/20/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameFiled : UIView

- (void)setupGameItems;
- (void)refreshGameItems;

@end
