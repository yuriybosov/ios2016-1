//
//  GameFiled.m
//  lesson 48_game_sapper
//
//  Created by Yuriy Bosov on 7/20/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "GameFiled.h"
#import "GameItem.h"

#define rowCount 5
#define columCount 5

#define difficultyLevel 0.20
#define bombCount (int)(rowCount * columCount * difficultyLevel)


#define lineWidth 2.f

@interface GameFiled ()
{
    NSMutableArray *gamesItems;
    NSUInteger currentCloseItems;
}

- (void)drawGrid;
- (void)drawItems;

@end

@implementation GameFiled

- (void)setupGameItems
{
    gamesItems = [NSMutableArray new];
    CGFloat itemWidht = self.frame.size.width / columCount;
    
    for (NSInteger i = 0; i < columCount; i++) {
        
        for (NSInteger j = 0; j < rowCount; j++) {
            
            CGRect frame = CGRectMake(i * itemWidht + lineWidth/2.f,
                                      j * itemWidht + 64 + lineWidth/2.f,
                                      itemWidht - lineWidth,
                                      itemWidht - lineWidth);
            GameItem *item = [GameItem new];
            item.state = GameItemStateClose;
            item.rect = frame;
            [gamesItems addObject:item];
        }
    }
    
    [self refreshGameItems];
}

- (void)refreshGameItems {
    for (GameItem *item in gamesItems) {
        item.state = GameItemStateClose;
        item.hasBomb = NO;
    }
    
    // задаем рандомно позиции бомб
    NSInteger i = bombCount;
    currentCloseItems = rowCount * columCount;
    while (i > 0) {
        
        NSUInteger randomIndex = arc4random() % (rowCount * columCount);
        
        GameItem *item = gamesItems[randomIndex];
        if (item.hasBomb == NO) {
            item.hasBomb = YES;
            i--;
            NSLog(@"bomb in %lu", randomIndex);
        }
    }
    
    // подсчитываем кол-во бомб рядом
    
    for (NSInteger i = 0; i < columCount; i++) {
        
        for (NSInteger j = 0; j < rowCount; j++) {
            
            NSUInteger currentIndex = i*rowCount + j;
            GameItem *item = gamesItems[currentIndex];
            
            NSUInteger count = 0;
            NSUInteger index = 0;
            GameItem *nearbyItem = nil;
            
            if (!item.hasBomb)
            {
                // top
                if  (j > 0)
                {
                    index = currentIndex - 1;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                // bottom
                if  (j < rowCount - 1)
                {
                    index = currentIndex + 1;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                // left
                if (i > 0)
                {
                    index = currentIndex - rowCount;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                // right
                if (i < columCount - 1)
                {
                    index = currentIndex + rowCount;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                //////////
                
                // top+left
                if  (j > 0 && i > 0)
                {
                    index = currentIndex - rowCount - 1;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                // top+right
                if  (j > 0 && i < columCount - 1)
                {
                    index = currentIndex + rowCount - 1;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                // bottom+left
                if  (j < rowCount - 1 && i > 0)
                {
                    index = currentIndex - columCount + 1;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
                
                // bottom+right
                if (j < rowCount - 1 && i < columCount - 1)
                {
                    index = currentIndex + rowCount + 1;
                    if (index < gamesItems.count) {
                        nearbyItem = gamesItems[index];
                        count = [nearbyItem hasBomb] ? count+1 : count;
                    }
                }
            }
            item.nearbyBombsCount = count;
        }
    }
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect  {
    
    // рисуем сетку
    [self drawGrid];
    
    // рисуем ячейки
    [self drawItems];
}

- (void)drawGrid {
    CGFloat itemWidht = self.frame.size.width / columCount;
    
    // задаем цвет грани
    [[UIColor cyanColor] setStroke];
    
    for (NSInteger i = 0; i < columCount; i++) {
        
        for (NSInteger j = 0; j < rowCount; j++) {
         
            CGRect frame = CGRectMake(i * itemWidht,
                                      j * itemWidht + 64,
                                      itemWidht,
                                      itemWidht);
            UIBezierPath *path = [UIBezierPath bezierPathWithRect:frame];
            [path setLineWidth:lineWidth];
            [path stroke];
        }
    }
}

- (void)drawItems {
    for (GameItem *item in gamesItems) {
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:item.rect];
        if (item.state == GameItemStateClose) {
            [[UIColor lightGrayColor] setFill];
        }
        else {
            if (item.hasBomb) {
                [[UIColor redColor] setFill];
            } else{
                [[UIColor whiteColor] setFill];
            }
        }

       [path fill];
        
        
        if (item.state == GameItemStateOpen &&
            !item.hasBomb) {
            NSDictionary *attr = @{NSFontAttributeName:[UIFont systemFontOfSize:item.rect.size.height/4]};
            [[NSString stringWithFormat:@"%lu", item.nearbyBombsCount] drawInRect:item.rect withAttributes:attr];
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    
    BOOL gameOwer = NO;
    BOOL win = NO;
    for (GameItem *item in gamesItems) {
     
        if (item.state == GameItemStateClose &&
            CGRectContainsPoint(item.rect, point)) {
            
            item.state = GameItemStateOpen;
            currentCloseItems--;
            
            if (item.hasBomb) {
                gameOwer = YES;
            } else if ((currentCloseItems - bombCount) == 0) {
                win = YES;
            }
            
            break;
        }
    }
    
    if (gameOwer || win) {
        for (GameItem *item in gamesItems) {
            item.state = GameItemStateOpen;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName: (gameOwer ? @"gameOwer" : @"gameWin") object:nil];
    }
    
    [self setNeedsDisplay];
}

@end
