//
//  ViewController.m
//  lesson 66_XML_Parser
//
//  Created by Yurii Bosov on 10/19/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "Place.h"

@interface ViewController () <NSXMLParserDelegate> {
    AFHTTPSessionManager *sessionManager;
    NSURLSessionDataTask *currentTask;
    NSMutableArray <Place *> *result; // типизированый массив
    
    NSString *currentElement;
    NSString *address_component_long_name;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:@"https://maps.googleapis.com"];
    sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    sessionManager.responseSerializer = [[AFXMLParserResponseSerializer alloc] init];
    
    NSString *searchText = @"Днепропетровск Кирова 76";
    NSDictionary *params = @{@"key":@"AIzaSyA95K7QmsKvc0eQgBHllEkg6hPSiIZ26bE",
                             @"address":searchText,
                             @"language":@"ru"};
    
    currentTask = [sessionManager GET:@"maps/api/geocode/xml"
                           parameters:params
                             progress:nil
                              success:^(NSURLSessionDataTask * _Nonnull task, NSXMLParser *responseObject)
                   {
                       if ([responseObject isKindOfClass:[NSXMLParser class]]) {
                           
                           responseObject.delegate = self;
                           [responseObject parse];
                           
                       }
                   }
                              failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                   {
                       ;// OS_ACTIVITY_MODE : disable
                   }];
}

#pragma mark - NSXMLParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    result = [[NSMutableArray alloc] init];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    for (id obj in result) {
        NSLog(@"%@", obj);
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    
    if (currentElement == nil)
        currentElement = elementName;
    else
        currentElement = [currentElement stringByAppendingPathComponent:elementName];
    
    NSLog(@"currentElement %@", currentElement);
    
    if ([currentElement isEqualToString: @"GeocodeResponse/result"]) {
        Place *place = [[Place alloc] init];
        [result addObject:place];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([currentElement isEqualToString:@"GeocodeResponse/result/formatted_address"]) {
        result.lastObject.address = string;
    }else if ([currentElement isEqualToString:@"GeocodeResponse/result/place_id"]){
        result.lastObject.placeID = string;
    }else if ([currentElement isEqualToString:@"GeocodeResponse/result/geometry/location/lat"]){
        CLLocationCoordinate2D coordinate = result.lastObject.coordinate;
        coordinate.latitude = [string doubleValue];
        result.lastObject.coordinate = coordinate;
    }else if ([currentElement isEqualToString:@"GeocodeResponse/result/geometry/location/lng"]){
        CLLocationCoordinate2D coordinate = result.lastObject.coordinate;
        coordinate.longitude = [string doubleValue];
        result.lastObject.coordinate = coordinate;
    }else if ([currentElement isEqualToString:@"GeocodeResponse/result/address_component/long_name"]){
        address_component_long_name = string;
    }else if ([currentElement isEqualToString:@"GeocodeResponse/result/address_component/type"]){
        if ([string isEqualToString:@"street_number"] &&
            address_component_long_name) {
            result.lastObject.streetNumber = address_component_long_name;
            address_component_long_name = nil;
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(nullable NSString *)namespaceURI qualifiedName:(nullable NSString *)qName {
}

@end
