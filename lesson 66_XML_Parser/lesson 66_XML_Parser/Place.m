//
//  Place.m
//  lesson 64_HTTP_GoogleAPI
//
//  Created by Yuriy Bosov on 10/12/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Place.h"

@implementation Place

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@, %f,%f, %@", _address, _placeID, _coordinate.latitude, _coordinate.longitude, _streetNumber];
}
@end
