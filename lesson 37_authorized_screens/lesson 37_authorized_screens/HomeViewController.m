//
//  HomeViewController.m
//  lesson 37_authorized_screens
//
//  Created by Yuriy Bosov on 6/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Home";
}

#pragma mark - Actions

- (IBAction)logoutButtonPressed:(id)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNavigationController"];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    appDelegate.window.rootViewController = vc;
}

@end
