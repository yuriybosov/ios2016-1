//
//  AlertControllerHelper.m
//  lesson 37_authorized_screens
//
//  Created by Yuriy Bosov on 6/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "AlertControllerHelper.h"

@implementation AlertControllerHelper

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message forController:(UIViewController *)controller
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [controller presentViewController:alert animated:YES completion:nil];
}

@end
