//
//  LoginViewController.m
//  lesson 37_authorized_screens
//
//  Created by Yuriy Bosov on 6/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "LoginViewController.h"
#import "AlertControllerHelper.h"
#import "AppDelegate.h"

@interface LoginViewController ()

@property (nonatomic, weak) IBOutlet UITextField *tfLogin;
@property (nonatomic, weak) IBOutlet UITextField *tfPassword;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Login";
}

- (void)validationTextFields
{
    if (_tfLogin.text.length == 0)
    {
        // show error alert
        [AlertControllerHelper showAlertWithTitle:@"error"message:@"login not filled" forController:self];
    }
    else if (_tfPassword.text.length == 0)
    {
        // show error alert
        [AlertControllerHelper showAlertWithTitle:@"error"message:@"password not filled" forController:self];
    }
    else
    {
        // go to Home Page
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeNavigationController"];
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        appDelegate.window.rootViewController = vc;
    }
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    [self validationTextFields];
}

- (IBAction)hideKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _tfLogin)
    {
        [_tfPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self validationTextFields];
    }
    
    return YES;
}

@end
