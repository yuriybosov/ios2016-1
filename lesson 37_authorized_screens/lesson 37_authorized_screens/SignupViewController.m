//
//  SignupViewController.m
//  lesson 37_authorized_screens
//
//  Created by Yuriy Bosov on 6/8/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "SignupViewController.h"

@interface SignupViewController ()

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Actiodns

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)singupButtonPressed:(id)sender
{
    //
}

- (IBAction)agreeChangeValue:(id)sender
{
    //
}

- (IBAction)hideKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

@end
