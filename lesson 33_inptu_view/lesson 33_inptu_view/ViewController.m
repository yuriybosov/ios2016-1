//
//  ViewController.m
//  lesson 33_inptu_view
//
//  Created by Yuriy Bosov on 5/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)showAlerViewWithTitle:(NSString *)title
                      message:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)doneButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    
    if (_tfPhoneNumber.text.length != 12)
    {
        // show error alert
        [self showAlerViewWithTitle:@"Error" message:@"Incorrect phone number"];
    }
    else if (_tfCardNumber.text.length != 16)
    {
        // show error alert
        [self showAlerViewWithTitle:@"Error" message:@"Incorrect card number"];
    }
    else
    {
        // success messege
        [self showAlerViewWithTitle:@"Success" message:@"Validation"];
    }
}

- (IBAction)textFildChangeValue:(UITextField *)sender
{
    BOOL enableBBI = _tfPhoneNumber.text.length > 0 && _tfCardNumber.text.length > 0;
    self.navigationItem.rightBarButtonItem.enabled = enableBBI;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    if (string.length > 0)
    {
        NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:@"^[0-9]+$" options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger count = [regExp numberOfMatchesInString:string
                                                   options:0
                                                     range:NSMakeRange(0, [string length])];
        result = (count > 0);
    }

    return result;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _tfPhoneNumber)
    {
        [_tfCardNumber becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

@end
