//
//  CustomClass2.m
//  lesson 12_1_strong_weak
//
//  Created by Yuriy Bosov on 3/2/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "CustomClass2.h"
#import "CustomClass1.h"

@implementation CustomClass2

- (void)dealloc
{
    NSLog(@"CustomClass2 dealloc");
}

@end
