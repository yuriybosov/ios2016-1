
#import <Foundation/Foundation.h>

@class CustomClass1;

@interface CustomClass2 : NSObject

@property (nonatomic, strong) CustomClass1 *customClass1;

@end
