
#import <Foundation/Foundation.h>

@class CustomClass2;

@interface CustomClass1 : NSObject

// assign - применяется для простых типов, доступно запись\чтение (assign по указан по умолчанию)
@property (nonatomic, assign) NSUInteger value1;

// readonly - применяется для простых и сложных типов, доступно только чтение
@property (nonatomic, readonly) NSUInteger value2;
@property (nonatomic, readonly) NSString *value3;

// weak (слабая ссылка) - применяется только для сложных типов, доступно чтение\запись.
// важно(!) - не ретейнит объект (т.е. не увеличивате ему счетчик ссылок)
// важно(!) - при удалении объекта weak свойство становится равным nil
@property (nonatomic, weak) NSString *value4;

// strong (сильная ссылка) - применяется только для сложных типов, доступно чтение\запись.
// важно(!) - ретейнит объект (т.е. увеличивате ему счетчик ссылок)
// важно(!) - это свойство удалится либо при удаление объкта, в котором это свойство указано, либо при "ручном" занулении свойств (self.value5 = nil;)
@property (nonatomic, strong) NSString *value5;


///
@property (nonatomic, weak) CustomClass2 *customClass2;

@end
