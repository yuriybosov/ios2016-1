//
//  CustomClass1.m
//  lesson 12_1_strong_weak
//
//  Created by Yuriy Bosov on 3/2/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "CustomClass1.h"
#import "CustomClass2.h"

@implementation CustomClass1

- (void)dealloc
{
    NSLog(@"CustomClass1 dealloc");
}

@end
