
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        // динамический словарь
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        // добавление объекта по ключу
        NSString *key1 = @"key1";
        NSString *obj1 = @"object1";
        [dict setObject:obj1 forKey:key1];
        
        NSString *key2 = @"key2";
        NSString *obj2 = @"object2";
        [dict setObject:obj2 forKey:key2];
        
        // удаление объекта по ключу
        [dict removeObjectForKey:key1];
        
        // удаление всех объектов (очистка словаря)
        [dict removeAllObjects];
    }
    return 0;
}
