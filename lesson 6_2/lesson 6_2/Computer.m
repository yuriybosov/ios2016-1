
#import "Computer.h"

@implementation Computer

- (NSString *)description
{
    return [NSString stringWithFormat:@"Computer: name = %@, price = %0.2f, power = %lu, \n%@, \n%@", _name, _price, _power, [_monitor description], [_hdd description]];
}

@end
