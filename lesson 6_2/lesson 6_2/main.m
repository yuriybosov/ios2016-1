
#import <Foundation/Foundation.h>
#import "Computer.h"


int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        Monitor *monitor = [[Monitor alloc] init];
        monitor.name = @"Apple Monitor";
        monitor.diagonal = 27;
        
        HardDrive *hdd = [[HardDrive alloc] init];
        hdd.name = @"toshiba";
        hdd.capacity = 500;
        hdd.speed = 3600;
        
        Computer *macBook = [[Computer alloc] init];
        macBook.name = @"Macbook Pro Retina 15 inch";
        macBook.price = 1600;
        macBook.power = 300;
        
        macBook.monitor = monitor;
        macBook.hdd = hdd;
        
        NSLog(@"%@", [macBook description]);
    }
    return 0;
}
