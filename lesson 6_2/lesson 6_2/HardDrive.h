
#import <Foundation/Foundation.h>

@interface HardDrive : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSUInteger speed;
@property (nonatomic) NSUInteger capacity;

@end
