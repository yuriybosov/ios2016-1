
#import <Foundation/Foundation.h>
#import "Monitor.h"
#import "HardDrive.h"

@interface Computer : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) CGFloat price;
@property (nonatomic) NSUInteger power;

@property (nonatomic, strong) Monitor *monitor;
@property (nonatomic, strong) HardDrive *hdd;

@end
