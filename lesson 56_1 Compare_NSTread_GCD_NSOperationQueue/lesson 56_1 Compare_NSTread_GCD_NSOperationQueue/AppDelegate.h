//
//  AppDelegate.h
//  lesson 56_1 Compare_NSTread_GCD_NSOperationQueue
//
//  Created by Yuriy Bosov on 9/19/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

