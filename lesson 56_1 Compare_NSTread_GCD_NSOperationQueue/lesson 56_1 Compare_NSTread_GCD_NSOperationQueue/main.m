//
//  main.m
//  lesson 56_1 Compare_NSTread_GCD_NSOperationQueue
//
//  Created by Yuriy Bosov on 9/19/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
