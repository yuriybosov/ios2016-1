//
//  DrawView.m
//  lesson 46_core_graphics
//
//  Created by Yuriy Bosov on 7/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "DrawView.h"

@implementation DrawView

// создавать графические прмитивы (рисовать что либо) можно только в этой функии
// 1. drawRect: вызывается системой в определенный случаях. Вызывать этот метод в коде безсмысленно
// 2. если есть необходимость выполнить перерисовку (т.е. вызвать метод drawRect:), то нужно вызвать метод setNeedsDisplay или setNeedsDisplayInRect: ()


- (void)drawRect:(CGRect)rect {
    
    // для отрисовки будем исользовать кондекст
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect); // Очистим context !!!!
    
    // общие настройки контекста:
    // задаем цвет заливки
    CGContextSetRGBFillColor(context, 1, 0, 0, 1);
    // задаем цвет контура
    CGContextSetRGBStrokeColor(context, 0, 0, 1, 1);
    // задаем ширину контура
    CGContextSetLineWidth(context, 3);
    
    // квадрат c заливкой
    CGContextFillRect(context, CGRectMake(10, 20, 50, 50));
    
    // квадрат c контуром
    CGContextStrokeRect(context, CGRectMake(70, 20, 50, 50));

    // круг(елипс) с заливкой
    CGContextFillEllipseInRect(context, CGRectMake(10, 80, 50, 50));
    
    // круг(елипс) с контуром
    CGContextStrokeEllipseInRect(context, CGRectMake(70, 80, 50, 50));
    
    // линия (для создание одно линии необходимо указать массив из 2-х точек)
    CGPoint pointsToLine[2] = {CGPointMake(10, 140), CGPointMake(120, 140)};
    CGContextStrokeLineSegments(context, pointsToLine, 2);

    // треугольник с заливкой. его рисуем с помощью трех линий, что бы создать три линии нужно создать массив из 6 точек
    CGPoint pointsToTriangle[6] = {CGPointMake(10, 200), CGPointMake(35, 150), CGPointMake(35, 150), CGPointMake(60, 200), CGPointMake(10, 200), CGPointMake(60, 200)};
    CGContextAddLines(context, pointsToTriangle, 6);
    CGContextFillPath(context);
    
    // теугольник с контуром
    CGPoint pointsToTriangle2[6] = {CGPointMake(70, 200), CGPointMake(95, 150), CGPointMake(95, 150), CGPointMake(120, 200), CGPointMake(70, 200), CGPointMake(120, 200)};
    CGContextStrokeLineSegments(context, pointsToTriangle2, 6);
    
    // сплайны с заливкой
    CGPoint bezierStart = {10, 260};
    CGPoint bezierEnd = {60,260};
    CGPoint bezierHelper1 = {10,220};
    CGPoint bezierHelper2 = {60,220};
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, bezierStart.x, bezierStart.y);
    CGContextAddCurveToPoint(context,
                             bezierHelper1.x, bezierHelper1.y,
                             bezierHelper2.x, bezierHelper2.y,
                             bezierEnd.x, bezierEnd.y);
    
    CGContextFillPath(context);
    
    // сплайн с контуром
    CGPoint bezierStart2 = {70, 260};
    CGPoint bezierEnd2 = {120,260};
    CGPoint bezierHelper21 = {70,220};
    CGPoint bezierHelper22 = {120,220};
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, bezierStart2.x, bezierStart2.y);
    CGContextAddCurveToPoint(context,
                             bezierHelper21.x, bezierHelper21.y,
                             bezierHelper22.x, bezierHelper22.y,
                             bezierEnd2.x, bezierEnd2.y);
    
    CGContextStrokePath(context);
    
    //
    bezierStart2 = CGPointMake(10, 320);
    bezierEnd2 = CGPointMake(120,320);
    bezierHelper21 = CGPointMake(50,270);
    bezierHelper22 = CGPointMake(80,370);
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, bezierStart2.x, bezierStart2.y);
    CGContextAddCurveToPoint(context,
                             bezierHelper21.x, bezierHelper21.y,
                             bezierHelper22.x, bezierHelper22.y,
                             bezierEnd2.x, bezierEnd2.y);
    
    CGContextStrokePath(context);

}

@end
