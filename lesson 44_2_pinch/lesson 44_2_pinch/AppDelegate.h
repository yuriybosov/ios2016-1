//
//  AppDelegate.h
//  lesson 44_2_pinch
//
//  Created by Yuriy Bosov on 7/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

