//
//  ViewController.m
//  lesson 44_2_pinch
//
//  Created by Yuriy Bosov on 7/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate>
{
    CGFloat lastScaleFactor;
    CGFloat lastRotateFactor;
    
    CGFloat deltaRotate;
}

@property (nonatomic, weak) IBOutlet UIView *rect;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    lastScaleFactor = 1;
}

- (IBAction)pich:(UIPinchGestureRecognizer *)sender {
    
    CGAffineTransform rotate = CGAffineTransformMakeRotation(lastRotateFactor);
    
    if (sender.scale > 1) { // увеличиваем размеры квадрата
        _rect.transform = CGAffineTransformScale(rotate,
                                                 lastScaleFactor + (sender.scale - 1),
                                                 lastScaleFactor + (sender.scale - 1));
    } else { // уменьшаем размеры
        _rect.transform = CGAffineTransformScale(rotate,
                                                 lastScaleFactor * sender.scale,
                                                 lastScaleFactor * sender.scale);
    }
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        if (sender.scale > 1){
            lastScaleFactor += (sender.scale - 1);
        } else {
            lastScaleFactor *= sender.scale;
        }
    }
}

- (IBAction)rotate:(UIRotationGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        lastRotateFactor -= sender.rotation;
        NSLog(@"===================");
    } else if (sender.state == UIGestureRecognizerStateChanged){
        lastRotateFactor = sender.rotation;
    }
    
    NSLog(@"%f", sender.rotation);
    
    CGAffineTransform scale = CGAffineTransformMakeScale(lastScaleFactor,
                                                         lastScaleFactor);
    _rect.transform = CGAffineTransformRotate(scale, lastRotateFactor);
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}

@end
