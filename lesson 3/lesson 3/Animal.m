
#import "Animal.h"

@implementation Animal

// реализация метода

- (void)showAnimalsInfo
{
    // тело метода (тело функции)
    // 
//    NSLog(@"имя = %@, возраст = %lu, пол = %i", self.name, self.age, self.gender);
    NSLog(@"имя = %@, возраст = %lu, пол = %i", _name, _age, _gender);
}

- (void)addAnimalAge:(NSUInteger)addAge
{
    _age = _age + addAge;
}

@end
