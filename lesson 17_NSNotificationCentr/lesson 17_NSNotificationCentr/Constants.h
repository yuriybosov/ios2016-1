//
//  Constants.h
//  lesson 17_NSNotificationCentr
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <Foundation/Foundation.h>

// объявление статических строк (они будут выступать в роли строкового идентификатора для сообщений)
static NSString *const kNotificationMessage1 = @"kNotificationMessage1";
static NSString *const kNotificationMessage2 = @"kNotificationMessage2";

#endif /* Constants_h */
