//
//  Sender.m
//  lesson 17_NSNotificationCentr
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Sender.h"
#import "Constants.h"

@implementation Sender

- (void)sendMessage
{
    // отправка сообщений
    // NSNotificationCenter - класс, который менеджирует отправкой сообщений и всеми объектами, которые подписались на рассылку этих сообщений
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessage1 object:self];
    
    // отправка сообщение с произвольными параметрами
    NSDictionary *params = @{@"key":@"value"};
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessage2 object:self userInfo:params];
}

@end
