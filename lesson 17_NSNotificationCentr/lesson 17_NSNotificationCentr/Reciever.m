//
//  Reciever.m
//  lesson 17_NSNotificationCentr
//
//  Created by Yuriy Bosov on 3/21/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Reciever.h"
#import "Constants.h"

// это класс получатель, он должет выполнить три действия при работе с NSNotificationCenter
/*
1. Подписаться на уведовления
2. Реализовать метод обработки уведомления
3. Отписаться от уведомлений
*/

@implementation Reciever

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Подписыавемся на уведовления
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        
        [nc addObserver:self
               selector:@selector(recieveNotification:)
                   name:kNotificationMessage1
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(recieveNotification2:)
                   name:kNotificationMessage2
                 object:nil];
    }
    return self;
}

- (void)dealloc
{
    // Отписываемся от уведомлений
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
}

// обработка уведомления
- (void)recieveNotification:(NSNotification *)aNotification
{
    NSLog(@"aNotification = %@", aNotification);
}

- (void)recieveNotification2:(NSNotification *)aNotification
{
    NSLog(@"aNotification = %@", aNotification);
}


@end
