//
//  ViewController.h
//  lesson 36
//
//  Created by Yuriy Bosov on 6/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *bbiBack;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *bbiForward;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *bbiRefresh;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;

@end

