//
//  ViewController.m
//  lesson_79_Localizations
//
//  Created by Yurii Bosov on 12/5/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UITextField *textField;
    IBOutlet UIButton *button;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // локализуем программно текста
    self.title = NSLocalizedString(@"Лекция №79", nil);
    textField.placeholder = NSLocalizedString(@"Введите ваше имя", nil);
    [button setTitle:NSLocalizedString(@"Авторизация", nil) forState:UIControlStateNormal];
    
}

@end
