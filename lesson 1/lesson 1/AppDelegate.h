//
//  AppDelegate.h
//  lesson 1
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

