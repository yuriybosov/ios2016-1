//
//  ViewController.h
//  lesson 1
//
//  Created by Yuriy Bosov on 1/25/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    // объявили поле класса,
    // интовая переменная, для хранения кол-во кликов
    int count;
}

// обьявление свойств
@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIButton *button;

// обьявление метода
- (IBAction)buttonDidPressed;

@end

