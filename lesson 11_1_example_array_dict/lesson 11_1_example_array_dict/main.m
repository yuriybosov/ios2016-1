
#import <Foundation/Foundation.h>
#import "Basket.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        Goods *g1 = [Goods createGoodsWith:@"хлеб"
                              categoryName:@"еда"
                                     price:@(7.65)
                                        ID:@(100)];
        
        Goods *g2 = [Goods createGoodsWith:@"хлеб"
                              categoryName:@"еда"
                                     price:@(7.65)
                                        ID:@(100)];
        
        Goods *g3 = [Goods createGoodsWith:@"молоко"
                              categoryName:@"еда"
                                     price:@(13.00)
                                        ID:@(200)];
        
        Basket *basket = [[Basket alloc] init];
        basket.money = 200;
        
        [basket addGoods:g1];
        [basket addGoods:g2];
        [basket addGoods:g3];
        
        [basket showInfon];
        
        [basket removeGoods:g3];
        
        [basket showInfon];

    }
    return 0;
}
