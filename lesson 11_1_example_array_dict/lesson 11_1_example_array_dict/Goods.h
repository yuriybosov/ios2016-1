//
//  Goods.h
//  lesson 11_1_example_array_dict
//
//  Created by Yuriy Bosov on 2/29/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

// класс, описывающий товар

@interface Goods : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *ID;

+ (id)createGoodsWith:(NSString *)name
         categoryName:(NSString *)categoryName
                price:(NSNumber *)price
                   ID:(NSNumber *)ID;

@end
