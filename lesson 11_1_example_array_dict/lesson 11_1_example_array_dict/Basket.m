//
//  Basket.m
//  lesson 11_1_example_array_dict
//
//  Created by Yuriy Bosov on 2/29/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Basket.h"

@implementation Basket

- (id)init //расширили метод инициализации, для того что бы проинициализировать поле класса goodsIDs
{
    self = [super init];
    if (self)
    {
        goodsIDs = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addGoods:(Goods *)goods
{
    // проверяем, позволяет ли наш лимит средств купить еще один товар
    CGFloat currentPrice = [self currentPrice];
    if (self.money - currentPrice - [goods.price floatValue] >= 0)
    {
        // проверяем наличие товара в нашем словаря (проверям по ключу, т.е. по ID товара)
        NSMutableArray *arrayOfGoods = [goodsIDs objectForKey:goods.ID];
        if (arrayOfGoods) // если массив существует, т.е. в корзине уже есть такой товар
        {
            [arrayOfGoods addObject:goods];
        }
        else // если массива нет, т.е. такой товар еще не был добавлен в корзину
        {
            //1 - создем массив
            arrayOfGoods = [[NSMutableArray alloc] init];
            //2 - добавляем в него товар
            [arrayOfGoods addObject:goods];
            //3 - сохраняем наш массив товаров в словаре, где ключ - ID товара, объект - массив товаров
            [goodsIDs setObject:arrayOfGoods forKey:goods.ID];
        }
    }
    else
    {
        NSLog(@"Товар купить нельзя, недостаточно средств!");
    }
}

- (void)removeGoods:(Goods *)goods
{
    // находим массив товаров по ID товара
    NSMutableArray *arrayOfGoods = [goodsIDs objectForKey:goods.ID];
    // удаляем товар
    [arrayOfGoods removeObject:goods];
    // если массив путь, удалить его из словаря
    if (arrayOfGoods.count == 0)
    {
        [goodsIDs removeObjectForKey:goods.ID];
    }
}

- (void)showInfon
{
    // делаем перебор в массиве товаров
    NSUInteger count = 0;
    for (NSArray *arrayOfGoods in goodsIDs.allValues)
    {
        // подсчет кол-ва одинаковых товаров
        count += arrayOfGoods.count;
    }
    
    // подсчет всей суммы товаров
    CGFloat totalPrice = [self currentPrice];
    
    NSLog(@"кол-во товаров = %lu, общая сумма = %0.2f", count, totalPrice);
}

- (CGFloat)currentPrice
{
    CGFloat totalPrice = 0;
    for (NSArray *arrayOfGoods in goodsIDs.allValues)
    {
        // суммируем цену товаров
        Goods *goods = [arrayOfGoods firstObject];
        totalPrice += [goods.price floatValue] * arrayOfGoods.count;
    }
    return totalPrice;
}

@end
