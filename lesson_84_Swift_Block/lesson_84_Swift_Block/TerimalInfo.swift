//
//  TerimalInfo.swift
//  lesson_84_Swift_Block
//
//  Created by Yurii Bosov on 12/28/16.
//  Copyright © 2016 ios. All rights reserved.
//

import UIKit

class TerimalInfo: NSObject {

    var type: String!
    var cityEN: String!
    var fullAddressEn: String!
    var placeRu: String!
    var latitude: Double!
    var longitude: Double!
    
    init(dictionary: [String:Any]) {
        
        self.type = dictionary["type"] as! String
        self.cityEN = dictionary["cityEN"] as! String
        self.fullAddressEn = dictionary["fullAddressEn"] as! String
        self.placeRu = dictionary["placeRu"] as! String
        
        let latString = dictionary["latitude"]
        let lngString = dictionary["longitude"]
        
        if latString != nil {
            self.latitude = Double(latString as! String)
        }
        
        if  lngString != nil {
            self.longitude = Double(lngString as! String)
        }
    }
}
