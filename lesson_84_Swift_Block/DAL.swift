//
//  DAL.swift
//  
//
//  Created by Yurii Bosov on 12/28/16.
//
//

import UIKit

class DAL: AFHTTPSessionManager {
    
    // create singleton
    static let sharedInstance: DAL = {
        
        let instance = DAL(baseURL: (URL(string: "https://api.privatbank.ua/p24api")))
        
        instance.requestSerializer = AFJSONRequestSerializer()
        instance.responseSerializer = AFJSONResponseSerializer()
        
        return instance
    }()
    
    func allTerminalsFor(cityName: String, complition:@escaping( _ data: [TerimalInfo]?,  _ errorMessage: String?) -> ()) -> Void {
        
        self.get("infrastructure?json&tso&address=&city=Dnipro",
                 parameters: nil,
                 progress: nil, success: {(task: URLSessionDataTask, data: Any?) in

                    var models = [TerimalInfo]()
                    
                    if let dictData: [String:Any] = data as? [String: Any] {
                        
                        if let modelsArray: [[String:Any]] = dictData["devices"] as? [[String:Any]] {
                            
                            for dictionary in modelsArray {
                                models.append(TerimalInfo.init(dictionary: dictionary))
                            }
                        }
                    }
                    complition(models, nil)
        },
                 failure: {(task: URLSessionDataTask?, error: Error) in
                    
                    complition(nil, error.localizedDescription)
        })
    }
}
