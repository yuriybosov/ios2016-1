//
//  MyObject2.m
//  lesson 41_UserDefault
//
//  Created by Yuriy Bosov on 6/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyObject2.h"

@implementation MyObject2

- (NSString *)description {
    return [NSString stringWithFormat:@"property1 = %@, object = %@", _property1, _object];
}

#pragma mark - NSCoding

//encodeWithCoder: вызывается при сохрании объекта
- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:_property1 forKey:@"_property1"];
    [aCoder encodeObject:_object forKey:@"_object"];
}

// initWithCoder: вызывается при load объекта
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    if (self) {
        _property1 = [aDecoder decodeObjectForKey:@"_property1"];
        _object = [aDecoder decodeObjectForKey:@"_object"];
    }
    return self;
}

@end
