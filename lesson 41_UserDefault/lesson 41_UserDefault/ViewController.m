//
//  ViewController.m
//  lesson 41_UserDefault
//
//  Created by Yuriy Bosov on 6/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "MyObject.h"
#import "MyObject2.h"


// обьявление строковых констант для ключей
static NSString *const kIntegerKey = @"kIntegerKey";
static NSString *const kBoolKey = @"kBoolKey";
static NSString *const kFloatKey = @"kFloatKey";

static NSString *const kStringKey = @"kStringKey";
static NSString *const kNumberKey = @"kNumberKey";
static NSString *const kCustomKey = @"kCustomKey";
static NSString *const kCustomKey2 = @"kCustomKey2";


@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Buttons Actions

- (IBAction)simpleSaveClicked:(id)sender {
    
    //
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    // сохрание int переменной
    NSInteger i = 10;
    [ud setInteger:i forKey:kIntegerKey];
    
    // сохрание float переменной
    CGFloat f = 23.5;
    [ud setFloat:f forKey:kFloatKey];
    
    // сохрание bool переменной
    BOOL b = YES;
    [ud setBool:b forKey:kBoolKey];
    
    // сохрание NSString переменной
    NSString *str = @"qwerqwer";
    [ud setObject:str forKey:kStringKey];
    
    // сохрание NSNumber переменной
    NSNumber *numb = @(100);
    [ud setObject:numb forKey:kNumberKey];
    
    // синхронизация (нужно вызывать всегда, если мы что либо меняли в UserDefaults)
    [ud synchronize];
    
    NSLog(@"Save simple values");
}

- (IBAction)simpleLoadClicked:(id)sender {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    // load int value
    NSInteger i = [ud integerForKey:kIntegerKey];
    NSLog(@"NSInteger = %li", (long)i);
    
    // load bool value
    BOOL b = [ud boolForKey:kBoolKey];
    NSLog(@"BOOL = %i", b);
    
    // load float value
    CGFloat f = [ud floatForKey:kFloatKey];
    NSLog(@"CGFloat = %f", f);
    
    // load NSString value
    NSString *str = [ud stringForKey:kStringKey];
    NSLog(@"NSString = %@", str);
    
    // load NSNumber value
    NSNumber *numb = [ud objectForKey:kNumberKey];
    NSLog(@"NSNumber = %@", numb);
}

- (IBAction)customSaveClicked:(id)sender {
    
    // сохрание custom object
    
    // 1. берем кастомный объект
    MyObject *obj = [[MyObject alloc] init];
    obj.property1 = @"qwer";
    obj.property2 = @"asdf";
    
    MyObject2 *obj2 = [[MyObject2 alloc] init];
    obj2.property1 = @"zxc";
    obj2.object = obj;
    
    // 2. нужно убедится в том, что данный объект реализует протокол NSCoding
    if ([obj conformsToProtocol:@protocol(NSCoding)] &&
        [obj2 conformsToProtocol:@protocol(NSCoding)])
    {
        // 3. Архивируем обьект в NSData
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:obj];
        NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:obj2];
        
        // 4. Сохраняем NSData в NSUserDefaults
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kCustomKey];
        [[NSUserDefaults standardUserDefaults] setObject:data2 forKey:kCustomKey2];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"Save custom object");
    }
    else
    {
        NSLog(@"Класс %@ не поддерживает протокол %@", NSStringFromClass([MyObject class]), NSStringFromProtocol(@protocol(NSCoding)));
    }
}

- (IBAction)customLoadClicked:(id)sender {
    
    // load custom object
    
    // 1. Из NSUserDefaults получаем NSData
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kCustomKey];
    NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:kCustomKey2];
    
    // 2. разархивируем NSData в кастомный объект
    MyObject *obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    MyObject *obj2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
    
    NSLog(@"obj = %@", obj);
    NSLog(@"obj2 = %@", obj2);
    
}

- (IBAction)clearButtonClicked:(id)sender {
    NSLog(@"clear data");
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
}


@end
