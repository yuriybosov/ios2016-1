//
//  MyObject2.h
//  lesson 41_UserDefault
//
//  Created by Yuriy Bosov on 6/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyObject.h"

@interface MyObject2 : NSObject <NSCoding>

@property (nonatomic, strong) NSString *property1;
@property (nonatomic, strong) MyObject *object;

@end
