//
//  DataProvider.h
//  lesson 62_URL_Request_Manager
//
//  Created by Yuriy Bosov on 10/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ComplitionBlock)(id data, NSError *error);

@interface DataProvider : NSObject

+ (DataProvider *)sharedInstance;

- (NSURLSessionTask *)filmsTodayWithComplition:(ComplitionBlock)block;
- (NSURLSessionTask *)filmsSoonWithComplition:(ComplitionBlock)block;

@end
