//
//  DataProvider.m
//  lesson 62_URL_Request_Manager
//
//  Created by Yuriy Bosov on 10/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "DataProvider.h"
#import "Film.h"

static NSString *const kBaseURLString = @"http://api.kinopoisk.cf";

@interface DataProvider () {
    NSURLSession *session;
}

- (NSURLSessionTask *)createTaskWithURL:(NSString *)baseUrlString
                                   path:(NSString *)path
                             httpMethod:(NSString *)httpMethod
                                 params:(NSDictionary *)params
                        completionBlock:(ComplitionBlock)block;

@end

@implementation DataProvider

+ (DataProvider *)sharedInstance {
    static DataProvider *dataProvider = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataProvider = [[DataProvider alloc] init];
    });
    
    return dataProvider;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //
        
        NSURLSessionConfiguration *configurator = [NSURLSessionConfiguration defaultSessionConfiguration];

        configurator.HTTPAdditionalHeaders = @{@"Accept":@"application/json"};
        
        session = [NSURLSession sessionWithConfiguration:configurator];
    }
    return self;
}

#pragma mark - Request

- (NSURLSessionTask *)createTaskWithURL:(NSString *)baseUrlString
                                   path:(NSString *)path
                             httpMethod:(NSString *)httpMethod
                                 params:(NSDictionary *)params
                        completionBlock:(ComplitionBlock)block {

    NSMutableString *urlString = [NSMutableString stringWithString:baseUrlString];
    
    if (path.length) {
        [urlString appendString:[NSString stringWithFormat:@"/%@", path]];
    }
    
    if (params.count) {
        
        if ([httpMethod isEqualToString:@"GET"]) {
            [urlString appendString:@"?"];
            __block NSMutableArray *paramsArray = [NSMutableArray array];
            [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                
                [paramsArray addObject:[NSString stringWithFormat:@"%@=%@",key, obj]];
                
            }];
            [urlString appendString:[paramsArray componentsJoinedByString:@"&"]];
        } else if ([httpMethod isEqualToString:@"POST"]) {
            // TODO
            // формирование body http post запроса
            
        }
    }
    
    NSLog(@"urlString: %@", urlString);
    NSLog(@"httpMethod: %@", httpMethod);
    NSLog(@"params: %@", params);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = httpMethod;
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        id responseData = nil;
        
        if (!error && data) {
            responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        
        if (block) {
            block(responseData, error);
        }
    }];
    
    return task;
}
#pragma mark - Get Films Request

- (NSURLSessionTask *)filmsTodayWithComplition:(ComplitionBlock)block {
    
    NSURLSessionTask *task = nil;
    task = [self createTaskWithURL:kBaseURLString
                              path:@"getTodayFilms"
                        httpMethod:@"GET"
                            params:@{@"key1":@"value1",
                                     @"key2":@"value2"}
                   completionBlock:^(id data, NSError *error) {
                     
                       // create Film models
                       NSArray *filmsArray = nil;
                       if ([data isKindOfClass:[NSDictionary class]]) {
                           filmsArray = [Film filmsWithArrayData:[data objectForKey:@"filmsData"]];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (block) {
                               block(filmsArray, error);
                           }
                       });
                 }];

    [task resume];
    return task;
}

- (NSURLSessionTask *)filmsSoonWithComplition:(ComplitionBlock)block {
    NSURLSessionTask *task = nil;
    
    task = [self createTaskWithURL:kBaseURLString
                              path:@"getSoonFilms"
                        httpMethod:@"GET"
                            params:nil
                   completionBlock:^(id data, NSError *error) {
                       
                       // create Film models
                       NSArray *filmsArray = nil;
                       if ([data isKindOfClass:[NSDictionary class]]) {
                           filmsArray = [Film filmsWithArrayData:[[data objectForKey:@"previewFilms"] firstObject]];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (block) {
                               block(filmsArray, error);
                           }
                       });
                   }];
    
    [task resume];
    return task;
}

@end
