//
//  NavigationController.h
//  lesson 62_URL_Request_Manager
//
//  Created by Yuriy Bosov on 10/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController

@end
