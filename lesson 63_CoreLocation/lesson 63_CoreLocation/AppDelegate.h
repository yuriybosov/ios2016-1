//
//  AppDelegate.h
//  lesson 63_CoreLocation
//
//  Created by Yuriy Bosov on 10/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

