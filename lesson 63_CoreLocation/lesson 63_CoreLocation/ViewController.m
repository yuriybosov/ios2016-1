//
//  ViewController.m
//  lesson 63_CoreLocation
//
//  Created by Yuriy Bosov on 10/10/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "HUD.h"

@import CoreLocation;

@interface ViewController () <UITextFieldDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    
    CLLocation* currentLocation;
}

@property (nonatomic, weak) IBOutlet UITextField *tfLatitude;
@property (nonatomic, weak) IBOutlet UITextField *tfLongitude;
@property (nonatomic, weak) IBOutlet UIButton *locationButton;
@property (nonatomic, weak) IBOutlet UIButton *weatherButton;

@property (nonatomic, strong) HUD *hudView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hudView = [[HUD alloc] init];
    
    self.title = @"Узнай погоду";
    
    _tfLatitude.enabled = NO;
    _tfLongitude.enabled = NO;
    _weatherButton.enabled = NO;
    
    [_locationButton setTitle:@"Получить текущие координаты" forState:UIControlStateNormal];
    [_weatherButton setTitle:@"Узнать погоду" forState:UIControlStateNormal];
}

#pragma mark - Actions

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)locationButtonClicked:(id)sender {
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        {
            // делаем запрос на разрешение трекать локацию
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            [locationManager requestWhenInUseAuthorization];
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            // доступ был дан ранее, можно трекать локацию
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            [locationManager requestLocation];
        }
            break;
            
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            // доступ был запрещен пользователем. показать алерт о необходимости включить локацию
        {
            currentLocation = nil;
            
            _tfLatitude.text = nil;
            _tfLongitude.text = nil;
            
            _tfLatitude.enabled = YES;
            _tfLongitude.enabled = YES;
            _weatherButton.enabled = NO;

            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Нам очень нужны выши координаты" message:@"очень очень нужны" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                // делаем открытие настроетк приложения
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
    
}

- (IBAction)weatherButtonClicked:(id)sender {
    if (currentLocation) {
        NSLog(@"currentLocation %@", currentLocation);
        
//        start request to
//        base url:
//        - http://api.openweathermap.org/
//    path:
//        - data/2.5/weather
//    params:
//        - lat
//        - lon
//        - APPID
        [self.hudView showInView:self.view];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
        
        NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&APPID=a0abbad1c1abd8ea4d64bf4b7c6a2726", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        request.HTTPMethod = @"GET";
        
        NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
               [self.hudView hide];
            });
            
            if (data && !error) {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSLog(@"weather data %@", dict);
            } else if (error){
                NSLog(@"error %@", error);
            }
            
        }];
        [task resume];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _tfLatitude) {
        [_tfLongitude becomeFirstResponder];
    } else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    _weatherButton.enabled = NO;
    
    if (_tfLatitude.text.length && _tfLongitude.text.length) {
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([_tfLatitude.text doubleValue], [_tfLongitude.text doubleValue]);
        if (CLLocationCoordinate2DIsValid(coordinate)) {
            currentLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
            _weatherButton.enabled = YES;
        }
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager requestLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    currentLocation = [locations lastObject];
    
    _tfLatitude.text = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
    _tfLongitude.text = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
    
    _tfLatitude.enabled = NO;
    _tfLongitude.enabled = NO;
    
    _weatherButton.enabled = YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManager didFailWithError %@", error);
    
    currentLocation = nil;
    
    _tfLatitude.text = nil;
    _tfLongitude.text = nil;
    
    _tfLatitude.enabled = YES;
    _tfLongitude.enabled = YES;
    _weatherButton.enabled = NO;
}

@end
