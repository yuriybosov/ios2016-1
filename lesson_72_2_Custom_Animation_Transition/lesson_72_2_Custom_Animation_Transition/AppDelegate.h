//
//  AppDelegate.h
//  lesson_72_2_Custom_Animation_Transition
//
//  Created by Yurii Bosov on 11/7/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

