//
//  ViewController.m
//  lesson_72_2_Custom_Animation_Transition
//
//  Created by Yurii Bosov on 11/7/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "CustomPushAnimator.h"

@interface ViewController () <UINavigationControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.delegate = self;
}

#pragma mark - UINavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    
    CustomPushAnimator *animator = [CustomPushAnimator new];
    animator.presenting = (operation == UINavigationControllerOperationPush);
    return animator;
}

@end
