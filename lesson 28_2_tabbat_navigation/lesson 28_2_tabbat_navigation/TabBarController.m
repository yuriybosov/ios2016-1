//
//  TabBarController.m
//  lesson 28_2_tabbat_navigation
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "TabBarController.h"

@implementation TabBarController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
