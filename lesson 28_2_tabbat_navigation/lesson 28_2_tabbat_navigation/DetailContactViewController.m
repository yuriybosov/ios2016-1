//
//  DetailContactViewController.m
//  lesson 28_2_tabbat_navigation
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "DetailContactViewController.h"

@interface DetailContactViewController ()

@end

@implementation DetailContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Contact detailed";
    // Do any additional setup after loading the view.
}

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
