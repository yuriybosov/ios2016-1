//
//  AppDelegate.h
//  lesson 28_2_tabbat_navigation
//
//  Created by Yuriy Bosov on 4/27/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

