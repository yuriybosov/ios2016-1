//
//  AppDelegate.h
//  lesson 24_1_navigation_bar
//
//  Created by Yuriy Bosov on 4/13/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

