//
//  AppDelegate.h
//  lesson 38_1 searchbar
//
//  Created by Yuriy Bosov on 6/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

