//
//  TodayViewController.swift
//  widget
//
//  Created by Yurii Bosov on 1/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extensionContext?.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {

        completionHandler(NCUpdateResult.newData)
    }
    
    @IBAction func openAppButtonClicked1() {
        let url = URL(string:"openApp://1")!
        self.extensionContext?.open(url, completionHandler: nil)
    }
    
    @IBAction func openAppButtonClicked2() {
        let url = URL(string:"openApp://2")!
        self.extensionContext?.open(url, completionHandler: nil)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = CGSize(width: 0, height: 110)
        } else {
            self.preferredContentSize = CGSize(width: 0, height: 275)
        }
    }
}
