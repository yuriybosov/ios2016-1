//
//  AppDelegate.h
//  lesson 42_2_UIPageViewController
//
//  Created by Yuriy Bosov on 6/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

