//
//  MyPageViewController.m
//  lesson 42_2_UIPageViewController
//
//  Created by Yuriy Bosov on 6/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MyPageViewController.h"
#import "ViewController.h"

@interface MyPageViewController() <UIPageViewControllerDataSource>
{
    NSArray *allControllers;
    NSInteger currentIndex;
}

@end

@implementation MyPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataSource = self;
    
    ViewController *vc1 = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc1.backgroundColor = [UIColor redColor];
    
    ViewController *vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc2.backgroundColor = [UIColor greenColor];
    
    ViewController *vc3 = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc3.backgroundColor = [UIColor yellowColor];
    
    ViewController *vc4 = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc4.backgroundColor = [UIColor purpleColor];
    
    ViewController *vc5 = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    vc5.backgroundColor = [UIColor orangeColor];
    
    allControllers = @[vc1, vc2, vc3, vc4, vc5];
    
    // устанавливаем первый экран для pagecontorller
    currentIndex = 0;
    [self setViewControllers:@[vc1] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [allControllers indexOfObject:viewController];
    if (index == 0 || index == NSNotFound) {
        return nil;
    }
    
    index--;
    currentIndex = index;
    
    return allControllers[currentIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [allControllers indexOfObject:viewController];
    if (index < allControllers.count - 1) {
        index++;
        currentIndex = index;
        return allControllers[currentIndex];
    }
    
    return nil;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController NS_AVAILABLE_IOS(6_0) {
    
    return [allControllers count];
    
}
- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return currentIndex;
}

@end
