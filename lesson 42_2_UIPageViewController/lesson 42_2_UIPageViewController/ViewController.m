//
//  ViewController.m
//  lesson 42_2_UIPageViewController
//
//  Created by Yuriy Bosov on 6/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UIView *contentView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentView.backgroundColor = self.backgroundColor;
}

@end
