//
//  KeyboardToolBar.h
//  lesson 31_textfield
//
//  Created by Yuriy Bosov on 5/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardToolBar : UIToolbar

@property (nonatomic, strong) NSArray *textFields;
@property (nonatomic, strong) UITextField *currentTextField;

+ (KeyboardToolBar *)createToolBar;

@end
