//
//  AppDelegate.h
//  lesson 31_textfield
//
//  Created by Yuriy Bosov on 5/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
