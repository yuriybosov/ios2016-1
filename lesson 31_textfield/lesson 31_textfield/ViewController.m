//
//  ViewController.m
//  lesson 31_textfield
//
//  Created by Yuriy Bosov on 5/18/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    inputFields = @[_tf1, _tf2, _tf3];
    townsArray = @[@"Киев",@"Львов",@"Днепропетровск"];
    
    toolBar = [KeyboardToolBar createToolBar];
    toolBar.textFields = inputFields;
    
    _tf1.inputAccessoryView = toolBar;
    _tf2.inputAccessoryView = toolBar;
    _tf3.inputAccessoryView = toolBar;
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 240)];
    pickerView.backgroundColor = [UIColor whiteColor];
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    _tf2.inputView = pickerView;
    
    
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (inputFields.lastObject == textField)
    {
        [textField resignFirstResponder];
    }
    else if ([inputFields containsObject:textField])
    {
        NSUInteger index = [inputFields indexOfObject:textField];
        UITextField *nextTextField = [inputFields objectAtIndex:index + 1];
        [nextTextField becomeFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    toolBar.currentTextField = textField;
    
    if (textField == _tf2 &&
        _tf2.text.length == 0)
    {
        textField.text = [townsArray firstObject];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    toolBar.currentTextField = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    
    if (textField == _tf1 && string.length > 0)
    {
        NSRegularExpression* regExp = [[NSRegularExpression alloc]initWithPattern:@"^[а-яіїєґ']+$" options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger count = [regExp numberOfMatchesInString:string
                                                   options:0
                                                     range:NSMakeRange(0, [string length])];
        result = (count > 0);
    }
    
    return result;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return townsArray.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [townsArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *str = [townsArray objectAtIndex:row];
    _tf2.text = str;
}

@end
