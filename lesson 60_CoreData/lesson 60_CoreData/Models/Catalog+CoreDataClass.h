//
//  Catalog+CoreDataClass.h
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

NS_ASSUME_NONNULL_BEGIN

@interface Catalog : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Catalog+CoreDataProperties.h"
