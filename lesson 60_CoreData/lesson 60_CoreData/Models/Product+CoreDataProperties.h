//
//  Product+CoreDataProperties.h
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Product+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

+ (NSFetchRequest<Product *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSNumber *price;
@property (nullable, nonatomic, copy) NSNumber *catalogIdentifier;
@property (nullable, nonatomic, retain) Catalog *catalog;

@end

NS_ASSUME_NONNULL_END
