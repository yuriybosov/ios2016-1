//
//  Catalog+CoreDataProperties.m
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Catalog+CoreDataProperties.h"

@implementation Catalog (CoreDataProperties)

+ (NSFetchRequest<Catalog *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Catalog"];
}

@dynamic title;
@dynamic identifier;
@dynamic parentIdentifier;
@dynamic products;
@dynamic subCatalogs;
@dynamic parentCatalog;

@end
