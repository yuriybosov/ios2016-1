//
//  ProductsController.m
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ProductsController.h"
#import "ProductCell.h"
#import "AppDelegate.h"

@interface ProductsController (){
    NSArray *dataSources;
}

@end

@implementation ProductsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.catalog.title;
    dataSources = [[self.catalog.products allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]]];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    
    Product *prod = [dataSources objectAtIndex:indexPath.row];
    cell.textLabel.text = prod.title;
    // add use NSNumberFormatter
    cell.detailTextLabel.text = [prod.price description];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
