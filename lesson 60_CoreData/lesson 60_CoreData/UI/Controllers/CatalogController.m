//
//  CatalogController.m
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "CatalogController.h"
#import "Catalog+CoreDataClass.h"
#import "Product+CoreDataClass.h"
#import "CatalogCell.h"
#import "AppDelegate.h"
#import "ProductsController.h"

@interface CatalogController () {
    NSArray *dataSources;
}

@property (nonatomic, strong) Catalog *catalog;

@end

@implementation CatalogController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.catalog == nil) {
        self.title = @"Main catalog";
        // show root catalog list
        
        NSFetchRequest *request = [Catalog fetchRequest];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]];
        request.predicate = [NSPredicate predicateWithFormat:@"parentIdentifier == 0 && (subCatalogs.@count != 0 || products.@count != 0)"];
        dataSources = [context() executeFetchRequest:request error:nil];
    } else {
        // show subcatalog list
        self.title = self.catalog.title;
        
        dataSources = [[self.catalog.subCatalogs allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]]];
    }

    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CatalogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CatalogCell" forIndexPath:indexPath];
    
    Catalog *catalog = [dataSources objectAtIndex:indexPath.row];
    cell.textLabel.text = catalog.title;
    
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Catalog *catalog = [dataSources objectAtIndex:indexPath.row];
    
    if (catalog.subCatalogs.count) {
        // если есть под каталоги - то отобразим экна с подкаталогами
        CatalogController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CatalogController"];
        vc.catalog = catalog;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        ProductsController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductsController"];
        vc.catalog = catalog;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
