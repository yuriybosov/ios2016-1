//
//  AppDelegate.m
//  lesson 60_CoreData
//
//  Created by Yuriy Bosov on 9/30/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "AppDelegate.h"
#import "Catalog+CoreDataClass.h"
#import "Product+CoreDataClass.h"

NSManagedObjectContext *context() {
    return ((AppDelegate*)[UIApplication sharedApplication].delegate).persistentContainer.viewContext;
}

void saveContext(){
    [(AppDelegate*)[UIApplication sharedApplication].delegate saveContext];
}


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // заполним базу данных
    [self insertObjectsToDB];
    
    return YES;
}

- (void)insertObjectsToDB {
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"createdDB"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdDB"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // root catalogs
        NSEntityDescription *catalogED = [NSEntityDescription entityForName:@"Catalog" inManagedObjectContext:context()];
        
        Catalog *catalogProduct = [[Catalog alloc] initWithEntity:catalogED insertIntoManagedObjectContext:context()];
        catalogProduct.title = @"Продукты";
        catalogProduct.identifier = @(1);
        
        Catalog *catalogAvto = [[Catalog alloc] initWithEntity:catalogED insertIntoManagedObjectContext:context()];
        catalogAvto.title = @"Авто";
        catalogAvto.identifier = @(2);
        
        Catalog *catalogInstrumenty = [[Catalog alloc] initWithEntity:catalogED insertIntoManagedObjectContext:context()];
        catalogInstrumenty.title = @"Иснтрументы";
        catalogInstrumenty.identifier = @(3);
        
        // sub catalog
        Catalog *catalogSoki = [[Catalog alloc] initWithEntity:catalogED insertIntoManagedObjectContext:context()];
        catalogSoki.title = @"Соки";
        catalogSoki.identifier = @(4);
        catalogSoki.parentIdentifier = @(1);
        [catalogProduct addSubCatalogsObject:catalogSoki];
        
        Catalog *catalogCrupi = [[Catalog alloc] initWithEntity:catalogED insertIntoManagedObjectContext:context()];
        catalogCrupi.title = @"Крупы";
        catalogCrupi.identifier = @(5);
        catalogCrupi.parentIdentifier = @(1);
        [catalogProduct addSubCatalogsObject:catalogCrupi];
        
        // products
        NSEntityDescription *productED = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:context()];
        
        // products for Авто
        Product *product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"Volvo";
        product.price = @(10000);
        product.catalogIdentifier = @(2);
        [catalogAvto addProductsObject:product];
        
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"VAZ 2101";
        product.price = @(1000000);
        product.catalogIdentifier = @(2);
        [catalogAvto addProductsObject:product];
        
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"ZAZ Dewoo";
        product.price = @(10000);
        product.catalogIdentifier = @(2);
        [catalogAvto addProductsObject:product];
        
        // products for Соки
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"Садочек";
        product.price = @(10);
        product.catalogIdentifier = @(4);
        [catalogSoki addProductsObject:product];
        
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"ОКЗДП";
        product.price = @(12);
        product.catalogIdentifier = @(4);
        [catalogSoki addProductsObject:product];
        
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"Сандора";
        product.price = @(8.5);
        product.catalogIdentifier = @(4);
        [catalogSoki addProductsObject:product];
        
        // products for Продукты
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"Греча";
        product.price = @(30.6);
        product.catalogIdentifier = @(5);
        [catalogCrupi addProductsObject:product];
        
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"Манка";
        product.price = @(12.5);
        product.catalogIdentifier = @(5);
        [catalogCrupi addProductsObject:product];
        
        product = [[Product alloc] initWithEntity:productED insertIntoManagedObjectContext:context()];
        product.title = @"Овсянка";
        product.price = @(6.5);
        product.catalogIdentifier = @(5);
        [catalogCrupi addProductsObject:product];
        
        saveContext();
    }
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"lesson_60_CoreData"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
