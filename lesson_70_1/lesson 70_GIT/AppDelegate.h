//
//  AppDelegate.h
//  lesson 70_GIT
//
//  Created by Yurii Bosov on 10/31/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//



#import <UIKit/UIKit.h>

// git@bitbucket.org:yuriybosov/lesson_70_1.git

@interface AppDelegate : UIResponder <UIApplicationDelegate>





@property (strong, nonatomic) UIWindow *window;


@end

