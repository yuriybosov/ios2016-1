//
//  TouchView.m
//  lesson 44_touch
//
//  Created by Yuriy Bosov on 7/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "TouchView.h"

@interface TouchView ()
{
    BOOL isSelectedRect;
    CGPoint delta;
}

- (void)selectRect;
- (void)deselectRect;
- (CGPoint)corectPointFrom:(CGPoint)fromPoint toPoint:(CGPoint)toPoint;

@property (nonatomic, weak) IBOutlet UIView *rect;

@end

@implementation TouchView

- (void)selectRect {
    isSelectedRect = YES;
    _rect.layer.borderColor = [[UIColor blackColor] CGColor];
    _rect.layer.borderWidth = 2;
}

- (void)deselectRect {
    isSelectedRect = NO;
    _rect.layer.borderColor = [[UIColor clearColor] CGColor];
    _rect.layer.borderWidth = 0;
}

- (CGPoint)corectPointFrom:(CGPoint)fromPoint toPoint:(CGPoint)toPoint {
    
    CGPoint corectPoint = fromPoint;
    
    if (fromPoint.x >= toPoint.x) // если двигаемся в лево
    {
        if (toPoint.x >= _rect.frame.size.width/2) {
            corectPoint.x = toPoint.x;
        } else {
            corectPoint.x = _rect.frame.size.width/2;
        }
    }
    else { // если двигаемся в право
        if (toPoint.x + _rect.frame.size.width/2 <= self.frame.size.width) {
            corectPoint.x = toPoint.x;
        } else {
            corectPoint.x = self.frame.size.width - _rect.frame.size.width/2;
        }
    }
    
    if (fromPoint.y > toPoint.y) { // если двигаемся в верх
        if (toPoint.y >= _rect.frame.size.height/2) {
            corectPoint.y = toPoint.y;
        } else {
            corectPoint.y = _rect.frame.size.height/2;
        }
    } else { // если двигаемся в низ
        
        if (toPoint.y + _rect.frame.size.height/2 <= self.frame.size.height) {
            corectPoint.y = toPoint.y;
        } else {
            corectPoint.y = self.frame.size.height - _rect.frame.size.height/2;
        }
    }
    
    return corectPoint;
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesBegan");
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    
    // CGRectContainsPoint - методо проверяет, находится ли точка в квадрате
    if (CGRectContainsPoint(_rect.frame, point)) {
        
        delta = CGPointMake(_rect.center.x - point.x,
                            _rect.center.y - point.y);
        [self selectRect];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesEnded");
    [self deselectRect];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesMoved");
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    
    // CGRectContainsPoint - методо проверяет, находится ли точка в квадрате
    if (CGRectContainsPoint(_rect.frame, point) &&
        isSelectedRect) {
        
        CGPoint newPoint = CGPointMake(point.x + delta.x,
                                       point.y + delta.y);
        
        CGPoint corectPoint = [self corectPointFrom:_rect.center
                                            toPoint:newPoint];
        
        _rect.center = corectPoint;
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesCancelled");
    [self deselectRect];
}

@end
