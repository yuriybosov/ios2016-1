//
//  ViewController.m
//  lesson 19
//
//  Created by Yuriy Bosov on 3/28/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

// методы инициализации UIViewController
// !!!! ВАЖНО
// нельзя в методах инициализации UIViewController создавать view-компоненты и работать с ними
// 1. метод initWithCoder: будет вызван у UIViewController в случае, если он был создан в storyboard
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        NSLog(@"%@", self.rect);
    }
    return self;
}

// 2. метод init будет вызван в случае, когда мы создаем UIViewController программно, т.е. с помощью кода, к примеру UIViewController* vc = [[UIViewController alloc] init];
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

// 3. метод initWithNibName:bundle: метод будет вызван в случае, когда UIViewController будет создан с помощью nib-файла
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        //
    }
    return self;
}

// жизненый цикл UIView
// 1. метод будет вызван при загрузке View
- (void)loadView
{
    [super loadView];
}

// 2. после loadView вызывается метод, который оповещает о том, что View было успешно загружено
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%@", self.rect);
    
    // задаем цвет UIView
    self.rect.backgroundColor = [UIColor colorWithRed:55/255.f green:95/255.f blue:251/255.f alpha:1.f];
    
    // скрыть \ отобразить view
    self.rect.hidden = NO;
    
    // поменять размеры view
    self.rect.frame = CGRectMake(0, 0, 100, 100);
}

// 3. метод, которые перед показам экрана
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

// 4. метод, который будет вызван после показа экрана
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

// 5. медот, который будет вызван перед тем, как экран будет скрыт
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

// 6. метод, который будет вызван после того, как экран был скрыт
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
