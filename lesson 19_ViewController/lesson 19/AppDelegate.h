//
//  AppDelegate.h
//  lesson 19
//
//  Created by Yuriy Bosov on 3/28/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

