//
//  AppDelegate.m
//  lesson 19
//
//  Created by Yuriy Bosov on 3/28/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

// application:didFinishLaunchingWithOptions: - метод, который вызывается один раз при запуске приложения
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    return YES;
}

@end
