//
//  ViewController.m
//  lesson 51_1_MultyplySelecdetImage
//
//  Created by Yuriy Bosov on 9/5/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@import Photos;


@interface ViewController ()

- (void)fetchAllPhotos;

@end



@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // get status
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    switch (status) {
        case PHAuthorizationStatusNotDetermined:
        {
            __weak ViewController *weakSelf = self;
            // start request
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                
                if (status == PHAuthorizationStatusAuthorized) {
                    // fetch all photos
                    [weakSelf fetchAllPhotos];
                }
                
            }];
        }
            break;
        
        case PHAuthorizationStatusDenied:
        case PHAuthorizationStatusRestricted:
        {
            // пользователь недал доступ к фотоблиотеке
        }
            break;
            
        case PHAuthorizationStatusAuthorized:
        {
            // fetch all photos
            [self fetchAllPhotos];
        }
            break;
    }
}

- (void)fetchAllPhotos {
    
    PHFetchResult *result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    
    [result enumerateObjectsUsingBlock:^(PHAsset*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [[PHImageManager defaultManager] requestImageDataForAsset:obj options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
            
            UIImage *image = [UIImage imageWithData:imageData];
            
            NSLog(@"image = %@", image);
        }];
    }];
    
}

@end
