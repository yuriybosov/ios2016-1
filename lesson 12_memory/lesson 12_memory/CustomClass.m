
#import "CustomClass.h"

@implementation CustomClass

// метод dealloc вызыватемя при удалении объекта
- (void)dealloc
{
//    [super dealloc] вызываем только если ARC выключен
    [super dealloc];
    NSLog(@"CustomClass dealloc");
}

@end
