//
//  main.m
//  lesson 12_memory
//
//  Created by Yuriy Bosov on 3/2/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomClass.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        CustomClass *obj1 = [[CustomClass alloc] init]; // создали объект, счетчик ссылок у него = 1
        NSLog(@"obj1 retainCount %lu", [obj1 retainCount]);
        
        [obj1 retain]; // программно увеличили счетчик ссылок на 1
        NSLog(@"obj1 retainCount %lu", [obj1 retainCount]);
        
        [obj1 release]; // программно уменьшили счетчик ссылок на 1
        NSLog(@"obj1 retainCount %lu", [obj1 retainCount]);

        //
        [obj1 release];
        obj1 = nil;
        
        // autorelease - отложеный вызов метода release
        CustomClass *obj2 = [[[CustomClass alloc] init] autorelease];
        NSLog(@"obj2 retainCount %lu", [obj2 retainCount]);
        
        //
        CustomClass *obj3 = [[CustomClass alloc] init];
        NSLog(@"obj3 retainCount %lu", [obj3 retainCount]);
        
        NSArray *array1 = [[NSArray alloc] initWithObjects:obj3, nil];
        NSLog(@"obj3 retainCount %lu", [obj3 retainCount]);
        
        [obj3 release];
        obj3 = nil;
        
        [array1 release];
        array1 = nil;
    }
    return 0;
}
