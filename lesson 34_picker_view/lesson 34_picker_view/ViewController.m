//
//  ViewController.m
//  lesson 34_picker_view
//
//  Created by Yuriy Bosov on 5/30/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"
#import "Brand.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    brands = [Brand allBrands];
    
    selectedBrand = [brands firstObject];
    selectedModel = [selectedBrand.models firstObject];
    
    // update model info text
    [self updateModelInfoText];
}

- (void)updateModelInfoText
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    nf.locale = [NSLocale localeWithLocaleIdentifier:@"RU"];
    nf.numberStyle = NSNumberFormatterCurrencyStyle;
    nf.currencySymbol = @"$";
    
    NSString *priceString = [nf stringFromNumber:selectedModel.price];
    
    NSString *modelInfoString = [NSString stringWithFormat:@"бренд %@\nмодель %@\nцена %@", selectedModel.brand.name ?: @"", selectedModel.name ?: @"", priceString ?: @""];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:modelInfoString];
    
    NSRange range;
    
    // brand name
    if (selectedModel.brand.name)
    {
        range = [modelInfoString rangeOfString:selectedModel.brand.name];
        if (range.location != NSNotFound && range.length > 0)
        {
            [attrString addAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:lbModelInfo.font.pointSize]}
                                range:range];
        }
    }
    
    // model name
    if (selectedModel.name)
    {
        range = [modelInfoString rangeOfString:selectedModel.name];
        if (range.location != NSNotFound && range.length > 0)
        {
            [attrString addAttributes:@{NSFontAttributeName : [UIFont italicSystemFontOfSize:lbModelInfo.font.pointSize]}
                                range:range];
        }
    }
    
    // price
    if (priceString)
    {
        range = [modelInfoString rangeOfString:priceString];
        if (range.location != NSNotFound && range.length > 0)
        {
            [attrString addAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:8], NSForegroundColorAttributeName : [UIColor redColor]}
                                range:range];
        }
    }
    
    lbModelInfo.attributedText = attrString;
}

#pragma mark - UIPickerViewDataSource, UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return brands.count;
    }
    else
    {
        return [brands objectAtIndex:component].models.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [brands objectAtIndex:row].name;
    }
    else
    {
        return [selectedBrand.models objectAtIndex:row].name;
    }
}

- (void)pickerView:(UIPickerView *)aPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        selectedBrand = [brands objectAtIndex:row];
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        
        selectedModel = [selectedBrand.models firstObject];
    }
    else
    {
        selectedModel = [selectedBrand.models objectAtIndex:row];
    }
    
    // update model info text
    [self updateModelInfoText];
}

@end
