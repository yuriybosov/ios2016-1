//
//  Brand.m
//  lesson 34_picker_view
//
//  Created by Yuriy Bosov on 5/30/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Brand.h"

@implementation Brand

+ (NSArray *)allBrands
{
    NSMutableArray *resulst = [[NSMutableArray alloc] init];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"dataList" ofType:@"plist"];
    NSArray *brandsData = [NSArray arrayWithContentsOfFile:filePath];
    
    for (NSDictionary *brandData in brandsData)
    {
        Brand *brand = [Brand new];
        brand.name = brandData[@"brandName"];
        
        NSMutableArray *models = [NSMutableArray new];
        
        for (NSDictionary *modelData in brandData[@"models"])
        {
            Model *model = [Model new];
            model.name = modelData[@"modelName"];
            model.price = modelData[@"modelPrice"];
            model.brand = brand;
            [models addObject:model];
        }
        
        brand.models = models;
        [resulst addObject:brand];
    }
    
    return resulst;
}

@end
