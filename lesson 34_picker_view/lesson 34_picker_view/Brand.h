//
//  Brand.h
//  lesson 34_picker_view
//
//  Created by Yuriy Bosov on 5/30/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Model.h"

@interface Brand : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray<Model *> *models;

+ (NSArray *)allBrands;

@end
