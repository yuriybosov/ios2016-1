//
//  ViewController.h
//  lesson 34_picker_view
//
//  Created by Yuriy Bosov on 5/30/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Brand.h"

@interface ViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UILabel *lbModelInfo;
    IBOutlet UIPickerView *pickerView;
    
    Model *selectedModel;
    Brand *selectedBrand;
    NSArray<Brand *> *brands;
}

@end

