//
//  ViewController.m
//  lesson_74_BestPractic_GoogleService
//
//  Created by Yurii Bosov on 11/19/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <AFNetworking.h>

@interface ViewController () <GMSMapViewDelegate>{
    IBOutlet GMSMapView *mapView;
    GMSPolyline *polyline;
    BOOL showFirstLocation;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    mapView.settings.myLocationButton = YES;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:mapView.myLocation.coordinate.latitude
                                                            longitude:mapView.myLocation.coordinate.longitude
                                                                 zoom:6];
    [mapView setCamera:camera];
    
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)aMapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"%f, %f", coordinate.latitude, coordinate.longitude);
    
    if (mapView.myLocation) {
        [self startRequestWithOriginCoordinate:mapView.myLocation.coordinate
                                   destination:coordinate];
    }
    
}

- (void)startRequestWithOriginCoordinate:(CLLocationCoordinate2D)origin destination:(CLLocationCoordinate2D)destination {
    
    NSString *originLocation = [NSString stringWithFormat:@"%f,%f", origin.latitude, origin.longitude];
    NSString *destinationLocation = [NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude];
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=AIzaSyC8G543_Jrm7PVFhuxbmcJbaX89EVtn1yU", originLocation, destinationLocation];
    
    [[AFHTTPSessionManager manager] GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //
        [self addDirectionWithRoutes:[responseObject objectForKey:@"routes"]];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //
        NSLog(@"%@", error);
        polyline.path = nil;
    }];
}

- (void)addDirectionWithRoutes:(NSArray *)routes {
    NSDictionary *rout = [routes firstObject];

    // bounds
    CLLocationCoordinate2D northeast;
    northeast.latitude = [[rout valueForKeyPath:@"bounds.northeast.lat"] doubleValue];
    northeast.longitude = [[rout valueForKeyPath:@"bounds.northeast.lng"] doubleValue];
    
    CLLocationCoordinate2D southwest;
    southwest.latitude = [[rout valueForKeyPath:@"bounds.southwest.lat"] doubleValue];
    southwest.longitude = [[rout valueForKeyPath:@"bounds.southwest.lng"] doubleValue];
    
    GMSCoordinateBounds *bouns = [[GMSCoordinateBounds alloc] initWithCoordinate:northeast coordinate:southwest];
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bouns withEdgeInsets:UIEdgeInsetsMake(100, 40, 40, 40)];
    
    [mapView animateWithCameraUpdate:update];
    
    // overview_polyline
    NSString *overviewPolyline = [rout valueForKeyPath:@"overview_polyline.points"];
    
    GMSPath *path = [GMSPath pathFromEncodedPath:overviewPolyline];

    if (!polyline) {
        polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeColor = [UIColor blueColor];
        polyline.strokeWidth = 5;
        polyline.map = mapView;
    }
    else {
        polyline.path = path;
    }
}

@end
