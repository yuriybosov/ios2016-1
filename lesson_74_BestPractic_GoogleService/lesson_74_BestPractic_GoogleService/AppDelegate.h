//
//  AppDelegate.h
//  lesson_74_BestPractic_GoogleService
//
//  Created by Yurii Bosov on 11/19/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

