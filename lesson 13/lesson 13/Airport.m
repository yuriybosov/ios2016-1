//
//  Airport.m
//  lesson 13
//
//  Created by Yuriy Bosov on 3/9/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Airport.h"

@implementation Airport

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        schedule = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)addFligt:(Flight *)flight
{
    [schedule setObject:flight forKey:flight.name];
}

- (void)addUser:(User *)user
       toFlight:(NSString *)flightName
{
    Flight *flight = [schedule objectForKey:flightName];
    if (flight)
    {
        if ([flight canAddUser])
        {
            [flight addUser:user];
        }
        else
        {
            NSLog(@"На рейсе %@ нет свободных мест", flight.name);
        }
    }
    else
    {
        NSLog(@"рейс %@ не найден", flightName);
    }
}

- (void)showInfo
{
    //1
    NSArray *allFlights = [schedule allValues];
    
    //2
    NSSortDescriptor *sortDescriptorByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    allFlights = [allFlights sortedArrayUsingDescriptors:@[sortDescriptorByName]];
    
    //3
    for (Flight *flight in allFlights)
    {
        NSLog(@"%@, время вылета - %@, %lu свободных мест", flight.name, flight.time, [flight countFreeSeats]);
    }
}

@end
