//
//  User.h
//  lesson 13
//
//  Created by Yuriy Bosov on 3/9/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSUInteger age;

+ (User*)createUserWithFirstName:(NSString *)firstName
                        lastName:(NSString *)lastName
                             age:(NSUInteger)age;

@end
