//
//  User.m
//  lesson 13
//
//  Created by Yuriy Bosov on 3/9/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "User.h"

// анонимная категория
// анонимная категория позволяет создать приватные методы, свойства и поля класса
@interface User ()
{
    NSUInteger identifier; // пример приватного поля класса
}

@property (nonatomic, assign) NSUInteger identifier2; // пример приватного свойства класса

// пример приватного объявления метода
- (id)initWithFirstName:(NSString *)firstName
               lastName:(NSString *)lastName
                    age:(NSUInteger)age;

@end

@implementation User

+ (User*)createUserWithFirstName:(NSString *)firstName
                        lastName:(NSString *)lastName
                             age:(NSUInteger)age
{
    User *user = [[User alloc] initWithFirstName:firstName
                                        lastName:lastName
                                             age:age];
    return user;
}

- (id)initWithFirstName:(NSString *)firstName
               lastName:(NSString *)lastName
                    age:(NSUInteger)age
{
    self = [super init];
    if (self)
    {
        _firstName = firstName;
        _lastName = lastName;
        _age = age;
    }
    return self;
}

@end
