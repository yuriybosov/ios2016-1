//
//  Airport.h
//  lesson 13
//
//  Created by Yuriy Bosov on 3/9/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flight.h"

@interface Airport : NSObject
{
    NSMutableDictionary *schedule;
}

@property (nonatomic, strong) NSString *name;

- (void)addFligt:(Flight *)flight;
- (void)addUser:(User *)user
       toFlight:(NSString *)flightName;
- (void)showInfo;

@end
