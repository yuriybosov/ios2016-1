//
//  Flight.m
//  lesson 13
//
//  Created by Yuriy Bosov on 3/9/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "Flight.h"

@implementation Flight

- (instancetype)initFlightWithName:(NSString *)name
                              time:(NSString *)time
                   maxCountOfUsers:(NSUInteger)maxCountOfUsers
{
    self = [super init];
    if (self)
    {
        _name = name;
        _time = time;
        _maxCountOfUsers = maxCountOfUsers;
        
        usersArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (BOOL)canAddUser
{
    return (_maxCountOfUsers - usersArray.count) > 0;
}

- (void)addUser:(User *)user
{
    if ([usersArray containsObject:user])
    {
        NSLog(@"Пользователь %@ уже добавлен в рейс", user.firstName);
    }
    else
    {
        [usersArray addObject:user];
    }
}

- (NSUInteger)countFreeSeats
{
    return _maxCountOfUsers - usersArray.count;
}

@end
