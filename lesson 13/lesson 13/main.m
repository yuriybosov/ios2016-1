
#import <Foundation/Foundation.h>
#import "Airport.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        // создали аэропорт
        Airport *airport = [[Airport alloc] init];
        airport.name = @"Dnipro";
       
        // создаем три рейса
        Flight *flight1 = [[Flight alloc] initFlightWithName:@"Kiev-Dnepr"
                                                        time:@"10:30" maxCountOfUsers:10];
        [airport addFligt:flight1];
        
        Flight *flight2 = [[Flight alloc] initFlightWithName:@"Dnepr-London"
                                                        time:@"16:15" maxCountOfUsers:20];
        [airport addFligt:flight2];
        
        Flight *flight3 = [[Flight alloc] initFlightWithName:@"Dnepr-Lvov"
                                                        time:@"20:45" maxCountOfUsers:8];
        [airport addFligt:flight3];
        
        [airport showInfo];
        
        // добавляем пассажиров на рейсы
        User *user1 = [User createUserWithFirstName:@"Yuriy"
                                           lastName:@"Bosov"
                                                age:30];
        [airport addUser:user1 toFlight:@"Dnepr-London"];
        
        User *user2 = [User createUserWithFirstName:@"Kostya"
                                           lastName:@"Syrbu"
                                                age:15];
        [airport addUser:user2 toFlight:@"Dnepr-London"];
        
        User *user3 = [User createUserWithFirstName:@"Vlad"
                                           lastName:@"Pavlenko"
                                                age:20];
        [airport addUser:user3 toFlight:@"Dnepr-Magadan"];
        
        [airport showInfo];
    }
    return 0;
}
