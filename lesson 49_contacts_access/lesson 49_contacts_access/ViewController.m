//
//  ViewController.m
//  lesson 49_contacts_access
//
//  Created by Yuriy Bosov on 8/29/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@import ContactsUI;
@import AddressBookUI;
@import AddressBook;
@import MessageUI;

@interface ViewController () <CNContactPickerDelegate, CNContactViewControllerDelegate, ABPeoplePickerNavigationControllerDelegate, ABNewPersonViewControllerDelegate, MFMailComposeViewControllerDelegate>

- (void)contactsListiOS8:(void (^)(NSArray *contacts))complitionBlock;
- (void)contactsListiOS9:(void (^)(NSArray *contacts))complitionBlock;
- (NSArray *)readContactsInStore:(CNContactStore *)store;

- (void)sendContactsByEmail:(NSArray *)contacts;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)contactListButtonClicked:(id)sender {
    
    // проверяем версию операционной системы
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
        // для iOS 9.0 и выше
        CNContactPickerViewController *contactController = [[CNContactPickerViewController alloc] init];
        contactController.delegate = self;
        
        [self presentViewController:contactController animated:YES completion:nil];
        
    } else{
        // для iOS ниже 9.0 версии
        // используем ABPeoplePickerNavigationController
        ABPeoplePickerNavigationController *nc = [ABPeoplePickerNavigationController new];
        nc.peoplePickerDelegate = self;
        [self presentViewController:nc animated:YES completion:nil];
    }
}

- (IBAction)addContactButtonClicked:(id)sender {
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) {
        
        // для iOS 9.0 и выше
        CNContactViewController *contactController = [CNContactViewController viewControllerForNewContact:nil];

        contactController.delegate = self;
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:contactController];
        [self presentViewController:nc animated:YES completion:nil];
        
    } else {
        // для iOS ниже 9.0 версии
        // используем ABPeoplePickerNavigationController
        
        ABNewPersonViewController *vc = [ABNewPersonViewController new];
        vc.newPersonViewDelegate = self;
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:nc animated:YES completion:nil];
    }
}

- (IBAction)shareContactButtonClicked:(id)sender {
    
    __weak ViewController *__weakSelf = self;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0){
        
        [self contactsListiOS9:^(NSArray *contacts) {
            
            [__weakSelf sendContactsByEmail:contacts];
            
        }];
    } else {
        [self contactsListiOS8:^(NSArray *contacts) {
           
            [__weakSelf sendContactsByEmail:contacts];
            
        }];
    }
}

- (void)sendContactsByEmail:(NSArray *)contacts {
    
    if (contacts.count) {
        
        // если есть контакты - отправляем их через email
        // проверяем, доступен ли сервис отправки почты
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController *emailControllet = [MFMailComposeViewController new];
            emailControllet.mailComposeDelegate = self;
            
            [emailControllet setSubject:@"My Contacts"];
            
            NSMutableString *messageBody = [NSMutableString new];
            
            for (CNContact *contact in contacts) {
                [messageBody appendFormat:@"%@ %@, %@\n", contact.givenName, contact.familyName, [[contact.phoneNumbers valueForKeyPath:@"value.stringValue"] componentsJoinedByString:@", "]];
            }
            
            [emailControllet setMessageBody:messageBody isHTML:NO];
            
            [self presentViewController:emailControllet animated:YES completion:nil];
            
        } else {
#warning TODO: add alert "please add email account"
        }
        
    } else{
#warning TODO: add alert "empty contact list"
    }
}

#pragma mark - CNContactPickerDelegate

// реализуем один из методов

//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(nonnull CNContact *)contact {
//    NSLog(@"%@", contact);
//}

//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContacts:(nonnull NSArray<CNContact *> *)contacts {
//    NSLog(@"%@", contacts);
//}

//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty {
//    NSLog(@"%@", contactProperty);
//}

#pragma mark - CNContactViewControllerDelegate

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(CNContact *)contact {
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  - ABPeoplePickerNavigationControllerDelegate

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [peoplePicker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  - ABNewPersonViewControllerDelegate

- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(nullable ABRecordRef)person {
    [newPersonView dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Contact List

- (void)contactsListiOS8:(void (^)(NSArray *))complitionBlock {
    // TODO
}
- (void)contactsListiOS9:(void (^)(NSArray *))complitionBlock {
    
    CNContactStore *store = [[CNContactStore alloc] init];
    
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    
    switch (status) {
        case CNAuthorizationStatusNotDetermined:
            // нужно запросить
            {
                __weak ViewController *__weakSelf = self;
                [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                    
                    if (granted) {
                        // доступ есть, можно вычитать контакты
                        NSArray *contacts = [__weakSelf readContactsInStore:store];
                        
                        if (complitionBlock)
                            complitionBlock(contacts);
                        
                    } else {
                        NSLog(@"request access error %@", error.localizedDescription);
                    }
                    
                }];
            }
            break;
            
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusRestricted:
            // нет доступа
            
            break;
            
        case CNAuthorizationStatusAuthorized:
            // доступ есть, можно вычитать контакты
            {
                NSArray *contacts = [self readContactsInStore:store];
                
                if (complitionBlock)
                    complitionBlock(contacts);
            }
            break;
    }
}

- (NSArray *)readContactsInStore:(CNContactStore *)store {
    
    NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:store.defaultContainerIdentifier];
    
    NSError *error = nil;
    NSArray *keyProperties = @[CNContactFamilyNameKey,
                               CNContactGivenNameKey,
                               CNContactPhoneNumbersKey];
    
    NSArray *contacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keyProperties error:&error];
    
    if (error) {
        NSLog(@"fetch error %@", error);
    }
    
    return contacts;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
