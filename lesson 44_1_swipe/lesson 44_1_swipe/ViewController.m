//
//  ViewController.m
//  lesson 44_1_swipe
//
//  Created by Yuriy Bosov on 7/6/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UIView *rect;

@end


@implementation ViewController

#pragma mark - Swipes

- (IBAction)swipe:(UISwipeGestureRecognizer *)sender {
    
    CGPoint point = _rect.center;
    
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        point = CGPointMake(_rect.frame.size.width/2,
                            point.y);
    } else if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        point = CGPointMake(self.view.frame.size.width - _rect.frame.size.width/2,
                            point.y);
    } else if (sender.direction == UISwipeGestureRecognizerDirectionDown) {
        point = CGPointMake(point.x,
                            self.view.frame.size.height - _rect.frame.size.height/2);
    } else if (sender.direction == UISwipeGestureRecognizerDirectionUp) {
        point = CGPointMake(point.x,
                            _rect.frame.size.height/2);
    }
    
    [UIView animateWithDuration:1 animations:^{
        _rect.center = point;
    }];
}

@end
