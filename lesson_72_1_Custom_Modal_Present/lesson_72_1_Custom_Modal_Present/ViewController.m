//
//  ViewController.m
//  lesson_72_1_Custom_Modal_Present
//
//  Created by Yurii Bosov on 11/7/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "ModalViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Button Actions

- (IBAction)buttonClicked:(UIButton *)sender {
    // show custom modal controller
    
    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"modalNC"];
    
    //1. добавить vc как дочерний контроллер текущего контролера
    [self addChildViewController:nc];
    //2. добавить вьюшку vc на вьюшку текщего котнролера
    [self.view addSubview:nc.view];
    
    //
    ModalViewController *vc = [nc.viewControllers objectAtIndex:0];
    vc.btn = sender;
    
    // делаем анимацию:
    nc.view.frame = sender.frame;
    nc.view.alpha = 0.5;
    
    [UIView animateWithDuration:2 animations:^{
        nc.view.frame = self.view.bounds;
        nc.view.alpha = 1;
    }];
}

@end
