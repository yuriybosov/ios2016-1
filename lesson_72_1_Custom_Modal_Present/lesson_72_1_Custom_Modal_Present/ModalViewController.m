//
//  ModalViewController.m
//  lesson_72_1_Custom_Modal_Present
//
//  Created by Yurii Bosov on 11/7/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()

@end

@implementation ModalViewController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)doneButtonClicked:(id)sender{
    
    // скываем текущий контроллер, который был показан не стандартным способом
    
    
    [UIView animateWithDuration:2 animations:^{
        
        self.navigationController.view.alpha = 0;
        self.navigationController.view.frame = self.btn.frame;
    
        
    } completion:^(BOOL finished) {
        // 1. сначало удаляем родительскую вью
        [self.navigationController.view removeFromSuperview];
        
        // 2. удаляем родительский контроллер
        [self.navigationController removeFromParentViewController];
    }];
}

@end
