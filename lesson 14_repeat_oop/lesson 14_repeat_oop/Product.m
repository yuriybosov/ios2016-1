

#import "Product.h"

@implementation Product

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _tags = [[NSMutableArray alloc] init];
        _characteristics = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (Product *)crateProductWithName:(NSString *)name
                            price:(NSNumber *)price
                     categoryName:(NSString *)categoryName
{
    Product *product = [[Product alloc] init];
    
    product.name = name;
    product.price = price;
    product.categoryName = categoryName;
    
    return product;
}

- (void)addTags:(NSArray *)tags andCharacteristics:(NSArray *)characteristics
{
    [_tags addObjectsFromArray:tags];
    [_characteristics addObjectsFromArray:characteristics];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"name - %@, price - %@, category - %@, tags - %@, characteristics - %@", _name, _price, _categoryName, [_tags componentsJoinedByString:@", "], [_characteristics componentsJoinedByString:@", "]];
}

@end
