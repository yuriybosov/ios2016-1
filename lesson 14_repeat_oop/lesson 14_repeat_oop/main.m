//
//  main.m
//  lesson 14_repeat_oop
//
//  Created by Yuriy Bosov on 3/14/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InternetShop.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        InternetShop *shop = [[InternetShop alloc] initWithName:@"COMFY"];
        
        Product *pr1 = [Product crateProductWithName:@"удочка №12"
                                               price:@(12.40)
                                        categoryName:@"рыбалка"];
        [pr1 addTags:@[@"отдых",@"рыба",@"улов"] andCharacteristics:@[@"3 метра",@"пять колен",@"два крючка",@"катушка иннерционая"]];
        
        Product *pr2 = [Product crateProductWithName:@"удочка №14"
                                               price:@(20.60)
                                        categoryName:@"рыбалка"];
        
        [pr2 addTags:@[@"отдых",@"рыба",@"улов"] andCharacteristics:@[@"5 метров",@"шесть колен",@"один крючек",@"катушка неиннерционая"]];
        
        Product *pr3 = [Product crateProductWithName:@"Велосипед"
                                               price:@(1200.00)
                                        categoryName:@"спорт"];
        
        [pr3 addTags:@[@"отдых",@"спорт",@"экстрим",@"прогулка"] andCharacteristics:@[@"12 скоростей",@"два колеса",@"вес 20 кг"]];
        
        [shop addProduct:pr1];
        [shop addProduct:pr2];
        [shop addProduct:pr3];
        
        [shop showInfoShop];
        [shop showCategoryInfo];
//        [shop removeProduct:pr1];
//        [shop removeProduct:pr2];
        
//        NSArray *temp = [shop findProductsByName:@"удо"];
//        NSLog(@"find result\n%@", [temp componentsJoinedByString:@"\n"]);

//        NSArray *temp = [shop findProductsByCategoryName:@"рыбалка"];
//        NSLog(@"find result\n%@", [temp componentsJoinedByString:@"\n"]);

//        NSArray *temp = [shop findProductsByTag:@"отдых"];
//        NSLog(@"find result\n%@", [temp componentsJoinedByString:@"\n"]);
        
        NSArray *temp = [shop findProductsByCharacteristic:@"5 метров"];
        NSLog(@"find result\n%@", [temp componentsJoinedByString:@"\n"]);

    }
    return 0;
}
