

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, readonly) NSMutableArray *tags;
@property (nonatomic, readonly) NSMutableArray *characteristics;

+ (Product *)crateProductWithName:(NSString *)name
                            price:(NSNumber *)price
                     categoryName:(NSString *)categoryName;

- (void)addTags:(NSArray *)tags andCharacteristics:(NSArray *)characteristics;

@end
