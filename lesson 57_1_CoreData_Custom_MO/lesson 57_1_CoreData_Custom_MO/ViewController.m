//
//  ViewController.m
//  lesson 57_1_CoreData_Custom_MO
//
//  Created by Yuriy Bosov on 9/21/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
