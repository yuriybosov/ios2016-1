//
//  Student+CoreDataProperties.m
//  lesson 57_1_CoreData_Custom_MO
//
//  Created by Yuriy Bosov on 9/21/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Student+CoreDataProperties.h"

@implementation Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Student"];
}

@dynamic studentID;
@dynamic firstName;
@dynamic lastName;
@dynamic dob;

@end
