//
//  Student+CoreDataProperties.h
//  lesson 57_1_CoreData_Custom_MO
//
//  Created by Yuriy Bosov on 9/21/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Student+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest;

@property (nonatomic) int64_t studentID;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSDate *dob;

@end

NS_ASSUME_NONNULL_END
