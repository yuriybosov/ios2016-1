//
//  ServerManager.h
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <AFNetworking.h>

typedef void(^ComplitionBlock)(id data, NSError *error);


@interface ServerManager : AFHTTPSessionManager

+ (ServerManager *)sharedManager;

- (void)ATMFromCity:(NSString *)city withComplitionBlock:(ComplitionBlock)complitionBlock;

@end
