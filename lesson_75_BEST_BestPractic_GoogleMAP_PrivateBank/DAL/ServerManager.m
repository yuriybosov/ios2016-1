//
//  ServerManager.m
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ServerManager.h"
#import "ATMModel.h"

@implementation ServerManager

+ (ServerManager *)sharedManager {
    static ServerManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.privatbank.ua/p24api"]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    });
    
    return manager;
}

- (void)ATMFromCity:(NSString *)city withComplitionBlock:(ComplitionBlock)complitionBlock {
    
    NSString *path = [NSString stringWithFormat:@"infrastructure?json&atm&city=%@", [city stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    
    [self GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error = nil;
        NSArray *result = [ATMModel arrayOfModelsFromDictionaries:[responseObject objectForKey:@"devices"] error:&error];
        
        if (complitionBlock) {
            complitionBlock(result, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (complitionBlock) {
            complitionBlock(nil, error);
        }
    }];
}

@end
