//
//  ATMCell.h
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATMCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *lbPlaceName;
@property (nonatomic, strong) IBOutlet UILabel *lbAddress;
@property (nonatomic, strong) IBOutlet UILabel *lbDistance;

@end
