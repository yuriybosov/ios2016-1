//
//  ATMMapController.m
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "ATMMapController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ServerManager.h"
#import "ATMModel.h"

@interface ATMMapController () {
    IBOutlet GMSMapView *map;
    NSArray < ATMModel *> *dataSources;
}

@end

@implementation ATMMapController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[ServerManager sharedManager] ATMFromCity:@"Днепр" withComplitionBlock:^(NSArray *data, NSError *error) {
        
        dataSources = data;
        for (ATMModel *model in dataSources) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            
            marker.title = model.placeName;
            marker.snippet = model.fullAddress;
            marker.position = CLLocationCoordinate2DMake([model.latitude doubleValue], [model.longitude doubleValue]);
            marker.map = map;
        }
    }];
}

@end
