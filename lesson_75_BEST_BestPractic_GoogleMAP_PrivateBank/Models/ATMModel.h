//
//  ATMModel.h
//  lesson_75_BEST_BestPractic_GoogleMAP_PrivateBank
//
//  Created by Yurii Bosov on 11/21/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ATMModel : JSONModel

@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) NSString *fullAddress;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;

@end
