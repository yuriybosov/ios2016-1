//
//  MenuController.m
//  lesson 43_SplipVeiwController
//
//  Created by Yuriy Bosov on 7/4/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MenuController.h"
#import "MenuCell.h"

@interface MenuController ()
{
    NSArray *dataSources;
    NSIndexPath *selectedIndexPath;
}

@end

@implementation MenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
//    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    self.title = @"Menu";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    dataSources = @[[MenuModel modelWithTitle:@"News" logo:@"news" storyboardID:@"newsController"],
                    [MenuModel modelWithTitle:@"Acticles" logo:@"articles" storyboardID:@"articlesController"],
                    [MenuModel modelWithTitle:@"Friends" logo:@"friends" storyboardID:@"friendsController"],
                    [MenuModel modelWithTitle:@"Favorites" logo:@"favorites" storyboardID:@"favoritesController"],
                    [MenuModel modelWithTitle:@"Settings" logo:@"settings" storyboardID:@"settingsController"]];
    
    selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([UIDevice currentDevice].userInterfaceIdiom != UIUserInterfaceIdiomPhone) {
        [self.tableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    
    [cell setupMenuModel:dataSources[indexPath.row]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedIndexPath = indexPath;
    // убираем выделение с ячейки (только для iphone)
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    MenuCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:cell.model.storyboardID];
    
    [self.splitViewController showDetailViewController:vc sender:nil];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(MenuCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.separatorTop.hidden = (indexPath.row == 0) ? YES : NO;
    cell.separatorBottom.hidden = (indexPath.row == dataSources.count - 1) ? YES : NO;
}

@end
