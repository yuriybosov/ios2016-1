//
//  MenuModel.m
//  lesson 43_SplipVeiwController
//
//  Created by Yuriy Bosov on 7/4/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MenuModel.h"

@implementation MenuModel

+ (MenuModel *)modelWithTitle:(NSString *)title
                         logo:(NSString *)logo
                 storyboardID:(NSString *)storyboardID
{
    MenuModel *model = [[MenuModel alloc] init];
    
    model.title = title;
    model.logo = logo;
    model.storyboardID = storyboardID;
    
    return model;
}

@end
