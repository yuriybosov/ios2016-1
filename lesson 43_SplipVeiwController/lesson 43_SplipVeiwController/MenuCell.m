//
//  MenuCell.m
//  lesson 43_SplipVeiwController
//
//  Created by Yuriy Bosov on 7/4/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "MenuCell.h"

@interface MenuCell ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *logoView;

@end


@implementation MenuCell

- (void)setupMenuModel:(MenuModel *)model {
    
    self.model = model;
    self.titleLabel.text = model.title;
    self.logoView.image = [UIImage imageNamed:model.logo];
}

@end
