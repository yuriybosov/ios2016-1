//
//  KeyBoard.m
//  lesson 38 custom_keyboard
//
//  Created by Yuriy Bosov on 6/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "KeyBoard.h"

@interface KeyBoard ()
{
    NSArray *symbolsArray;
}

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *keyboardButtons;

@end

@implementation KeyBoard

+ (KeyBoard *)crateKeyBoard
{
    KeyBoard *keyboard = [[[NSBundle mainBundle] loadNibNamed:@"KeyBoard" owner:nil options:nil] firstObject];
    
    return keyboard;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    symbolsArray = @[@"1",@"2",@"3",@"4",@"5",@"6"];
    if (symbolsArray.count == self.keyboardButtons.count)
    {
        for (NSUInteger i = 0; i <  symbolsArray.count; i++)
        {
            UIButton *btn = self.keyboardButtons[i];
            NSString *symbol = symbolsArray[i];
            [btn setTitle:symbol forState:UIControlStateNormal];
        }
    }
}

#pragma mark - Actions

- (IBAction)backspaseButtonPressed:(UIButton *)sender {
    [self.delegate backspaseButtonPressed];
}

- (IBAction)returnButtonPressed:(UIButton *)sender {
    [self.delegate returnButtonPressed];
}

- (IBAction)keyBoardButtonPressed:(UIButton *)sender {
    [self.delegate didEntrySymbol:[sender titleForState:UIControlStateNormal]];
}

@end
