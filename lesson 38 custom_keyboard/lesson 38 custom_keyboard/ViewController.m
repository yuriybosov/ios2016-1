//
//  ViewController.m
//  lesson 38 custom_keyboard
//
//  Created by Yuriy Bosov on 6/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "ViewController.h"
#import "KeyBoard.h"

@interface ViewController () <KeyBoardProtocol>

@property (nonatomic, weak) IBOutlet UITextField *textField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    KeyBoard *keyBoard = [KeyBoard crateKeyBoard];
    keyBoard.delegate = self;
    self.textField.inputView = keyBoard;
}

#pragma mark - KeyBoardProtocol

-  (void)backspaseButtonPressed {
    if (self.textField.text.length > 0) {
        self.textField.text = [self.textField.text substringToIndex:self.textField.text.length - 1];
    }
}

-  (void)returnButtonPressed {
    [self.textField resignFirstResponder];
}

-  (void)didEntrySymbol:(NSString *)symbol {
    self.textField.text = [self.textField.text stringByAppendingString:symbol];
}
@end
