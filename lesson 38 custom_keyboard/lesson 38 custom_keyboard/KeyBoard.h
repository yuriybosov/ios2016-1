//
//  KeyBoard.h
//  lesson 38 custom_keyboard
//
//  Created by Yuriy Bosov on 6/13/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KeyBoardProtocol <NSObject>

- (void)backspaseButtonPressed;
- (void)returnButtonPressed;
- (void)didEntrySymbol:(NSString *)symbol;

@end

@interface KeyBoard : UIView

@property (nonatomic, weak) id<KeyBoardProtocol> delegate;

+ (KeyBoard *)crateKeyBoard;

@end
