//
//  ViewController.m
//  lesson 20_tableview
//
//  Created by Yuriy Bosov on 3/30/16.
//  Copyright © 2016 ios courses. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // указываем табличке, что наш viewController будет для нее dataSource-делегатом
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
 
    // убираем сепараторы в таблице
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

// метод отвечает за то, какие ориентации будут поддерживаться текущим экраном
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark - UITableViewDataSource

// возвращает кол-во секций для таблички (по умолчанию 1 секция)
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

// возвращает кол-во ячеек для конкретной секции.номер секции приходит в параметре section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if (section == 0)
    {
        count = 4;
    }
    else if (section == 1 || section == 2)
    {
        count = 2;
    }
    else if (section == 3)
    {
        count = 3;
    }
    
    return count;
}

// метод возвращает ячейку для определеного indexPath. 
// в этом методе нужно задавать данные для ячейки (т.е. задавать тот контент, который она будет отображать)
//indexPath - содержит номер секции и номер ячейки
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *ID = [NSString stringWithFormat:@"type%li", indexPath.section + 1];
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:ID];

    cell.textLabel.text = [NSString stringWithFormat:@"Section = %li",indexPath.section];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Row = %li",indexPath.row];

    // выключим выделение ячеек для первой секции
    if (indexPath.section == 0)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if (indexPath.section == 1) // зададим кастомый цвет выделения для ячеек второй секции
    {
            cell.selectedBackgroundView = [[UIView alloc] init];
            cell.selectedBackgroundView.backgroundColor = [UIColor blueColor];
    }
    else // если не первая секция - то вернем выделение ячейки при нажатии
    {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath %@", indexPath);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didDeselectRowAtIndexPath %@", indexPath);
}

@end
