//
//  Film+CoreDataProperties.h
//  lesson 61_URL_request
//
//  Created by Yuriy Bosov on 10/3/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import "Film+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Film (CoreDataProperties)

+ (NSFetchRequest<Film *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSString *nameRU;
@property (nullable, nonatomic, copy) NSString *nameEN;
@property (nullable, nonatomic, copy) NSString *year;
@property (nullable, nonatomic, copy) NSNumber *isIMAX;
@property (nullable, nonatomic, copy) NSNumber *isNew;
@property (nullable, nonatomic, copy) NSString *posterURL;
@property (nullable, nonatomic, copy) NSDate *premiereRU;
@property (nullable, nonatomic, copy) NSString *videoURL_HD;
@property (nullable, nonatomic, copy) NSString *videoURL_SD;
@property (nullable, nonatomic, copy) NSString *videoURL_LOW;

@end

NS_ASSUME_NONNULL_END
