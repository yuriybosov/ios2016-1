//
//  Film+CoreDataClass.h
//  lesson 61_URL_request
//
//  Created by Yuriy Bosov on 10/3/16.
//  Copyright © 2016 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Film : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Film+CoreDataProperties.h"
